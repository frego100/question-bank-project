SELECT 
    u.id as user_id,
    r.id as role_id,
    r.name as role_name,
    rp.role_id as r_id,
    p.name as permission_name
FROM users as u
LEFT JOIN roles as r ON r.id = u.role_id
LEFT JOIN role_permissions as rp ON r.id = rp.role_id
LEFT JOIN permissions as p ON rp.permission_id = p.id