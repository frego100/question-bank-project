# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Area.create(name: 'Biomédicas')
Area.create(name: 'Ingenierías')
Area.create(name: 'Sociales')

user_admin = User.create(first_name: 'Juan Carlos',
                         last_name: 'Juarez Bueno', email: 'bueno@unsa.edu.pe',
                         password: 'Testing1*', password_confirmation: 'Testing1*')

user_formulador = User.create(first_name: 'Formulador Nombre',
                              last_name: 'Formulador Apellido', email: 'formulador@unsa.edu.pe',
                              password: 'Formulador1*', password_confirmation: 'Formulador1*')

user_operador = User.create(first_name: 'Operador Nombre',
                            last_name: 'Operador Apellido', email: 'operador@unsa.edu.pe',
                            password: 'Operador1*', password_confirmation: 'Operador1*')

user_revisor_pertinencia = User.create(first_name: 'Pertinencia Nombre',
                                       last_name: 'Pertinencia Apellido', email: 'pertinencia@unsa.edu.pe',
                                       password: 'Pertinencia1*', password_confirmation: 'Pertinencia1*')

user_revisor_estilo = User.create(first_name: 'Estilo Nombre',
                                  last_name: 'Estilo Apellido', email: 'estilo@unsa.edu.pe',
                                  password: 'Estilo1*', password_confirmation: 'Estilo1*')

# Basic permissions are defined using <controller>-<action>
# There will be another kind of permission referred to a group of the first kind of permissions

def create_simple_permission(name)
  Permission.find_or_create_by(name: name) do |p|
    p.scope = 0
  end
end

def create_complex_permission(name)
  Permission.find_or_create_by(name: name) do |p|
    p.scope = 1
  end
end

def get_permission(name)
  Permission.find_by(name: name)
end

all_permissions = [
  create_simple_permission('user_index'),
  create_simple_permission('user_show'),
  create_simple_permission('user_edit'),
  create_simple_permission('user_new'),
  create_simple_permission('user_create'),
  create_simple_permission('user_update'),
  create_simple_permission('user_destroy'),
  create_simple_permission('user_password_edit'),
  create_simple_permission('user_update_password'),
  create_simple_permission('user_pertinency_reviewers'),
  create_simple_permission('user_format_reviewers'),

  create_simple_permission('role_index'),
  create_simple_permission('role_show'),
  create_simple_permission('role_edit'),
  create_simple_permission('role_new'),
  create_simple_permission('role_create'),
  create_simple_permission('role_update'),
  create_simple_permission('role_destroy'),

  create_simple_permission('question_index'), # To)
  create_simple_permission('question_index_operator'), # To)
  create_simple_permission('question_index_formulator'), # To)
  create_simple_permission('question_index_reviewer'), # To)
  create_simple_permission('question_show'),  # 35 #Formulador, Operador, Revisor de estilo y de pertinencia (filtrar según estado de la pregun)
  create_simple_permission('question_edit'),  # 36 #Formulador, Revisor de Est)
  create_simple_permission('question_update'),
  create_simple_permission('question_new'), # Formula)
  create_simple_permission('question_create'),
  create_simple_permission('question_destroy'), # Formulador (según estado de la pregunta, Estado ==)
  create_simple_permission('question_assign_question'),
  create_simple_permission('question_approve_question'),
  create_simple_permission('question_reject_question'),
  create_simple_permission('question_send_question_to_review'),
  create_simple_permission('question_observe_question'),
  create_simple_permission('question_formulator_status'),
  create_simple_permission('question_reviewer_status'),

  create_simple_permission('comment_index'),
  create_simple_permission('comment_show'),
  create_simple_permission('comment_create'),
  create_simple_permission('comment_update'),
  create_simple_permission('comment_destroy'),

  create_simple_permission('call_index'),
  create_simple_permission('call_show'),
  create_simple_permission('call_edit'),
  create_simple_permission('call_new'),
  create_simple_permission('call_create'),
  create_simple_permission('call_update'),
  create_simple_permission('call_destroy'),
  create_simple_permission('call_add_formulator'),
  create_simple_permission('call_add_pertinency_reviewer'),
  create_simple_permission('call_add_format_reviewer'),
  create_simple_permission('call_switch_status'),

  create_simple_permission('category_list'),
  create_simple_permission('category_edit_tree'),
  create_simple_permission('category_index'),
  create_simple_permission('category_create'),
  create_simple_permission('category_update'),
  create_simple_permission('category_destroy'),

  create_simple_permission('entrance_exam_index'),
  create_simple_permission('entrance_exam_show'),
  create_simple_permission('entrance_exam_edit'),
  create_simple_permission('entrance_exam_create'),
  create_simple_permission('entrance_exam_update'),
  create_simple_permission('entrance_exam_destroy'),
  create_simple_permission('entrance_exam_add_question'),
  create_simple_permission('entrance_exam_remove_question'),

  create_complex_permission('complex_formulator_permission'),
  create_complex_permission('complex_pertinency_reviewer_permission'),
  create_complex_permission('complex_format_reviewer_permission'),
  create_complex_permission('complex_operator_permission'),
  create_complex_permission('complex_super_admin_permission'),
]

super_admin_permissions = [

  get_permission('user_index'),
  get_permission('user_show'),
  get_permission('user_edit'),
  get_permission('user_new'),
  get_permission('user_create'),
  get_permission('user_update'),
  get_permission('user_destroy'),
  get_permission('user_password_edit'),
  get_permission('user_update_password'),
  get_permission('user_pertinency_reviewers'),
  get_permission('user_format_reviewers'),

  get_permission('role_index'),
  get_permission('role_show'),
  get_permission('role_edit'),
  get_permission('role_new'),
  get_permission('role_create'),
  get_permission('role_update'),
  get_permission('role_destroy'),

  get_permission('question_index'), # To)
  get_permission('question_index_operator'), # To)
  get_permission('question_index_formulator'), # To)
  get_permission('question_index_reviewer'), # To)
  get_permission('question_show'),  # 35 #Formulador, Operador, Revisor de estilo y de pertinencia (filtrar según estado de la pregun)
  get_permission('question_edit'),  # 36 #Formulador, Revisor de Est)
  get_permission('question_update'),
  get_permission('question_new'), # Formula)
  get_permission('question_create'),
  get_permission('question_destroy'), # Formulador (según estado de la pregunta, Estado ==)
  get_permission('question_assign_question'),
  get_permission('question_approve_question'),
  get_permission('question_reject_question'),
  get_permission('question_send_question_to_review'),
  get_permission('question_observe_question'),
  get_permission('question_formulator_status'),
  get_permission('question_reviewer_status'),

  get_permission('comment_index'),
  get_permission('comment_show'),
  get_permission('comment_create'),
  get_permission('comment_update'),
  get_permission('comment_destroy'),

  get_permission('call_index'),
  get_permission('call_show'),
  get_permission('call_edit'),
  get_permission('call_new'),
  get_permission('call_create'),
  get_permission('call_update'),
  get_permission('call_destroy'),
  get_permission('call_add_formulator'),
  get_permission('call_add_pertinency_reviewer'),
  get_permission('call_add_format_reviewer'),
  get_permission('call_switch_status'),

  get_permission('category_list'),
  get_permission('category_edit_tree'),
  get_permission('category_index'),
  get_permission('category_create'),
  get_permission('category_update'),
  get_permission('category_destroy'),

  get_permission('entrance_exam_index'),
  get_permission('entrance_exam_show'),
  get_permission('entrance_exam_edit'),
  get_permission('entrance_exam_create'),
  get_permission('entrance_exam_update'),
  get_permission('entrance_exam_destroy'),
  get_permission('entrance_exam_add_question'),
  get_permission('entrance_exam_remove_question'),

  get_permission('complex_super_admin_permission')
]

formulador_permissions = [
  get_permission('question_show'),
  get_permission('question_edit'),
  get_permission('question_update'),
  get_permission('question_new'),
  get_permission('question_create'),
  get_permission('question_destroy'),
  get_permission('question_send_question_to_review'),
  get_permission('complex_formulator_permission')
]

operador_permissions = [
  get_permission('user_password_edit'),
  get_permission('user_update_password'),

  get_permission('question_index'),
  get_permission('question_assign_question'),
  get_permission('question_show'),
  get_permission('user_pertinency_reviewers'),
  get_permission('user_format_reviewers'),
  get_permission('comment_index'),
  get_permission('comment_show'),

  get_permission('entrance_exam_index'),
  get_permission('entrance_exam_show'),
  get_permission('entrance_exam_edit'),
  get_permission('entrance_exam_create'),
  get_permission('entrance_exam_update'),
  get_permission('entrance_exam_destroy'),
  get_permission('entrance_exam_add_question'),
  get_permission('entrance_exam_remove_question'),

  get_permission('complex_operator_permission')
]

revisor_pertinencia_permissions = [
  get_permission('user_password_edit'),
  get_permission('user_update_password'),

  get_permission('question_show'),
  get_permission('question_approve_question'),
  get_permission('question_reject_question'),
  get_permission('question_observe_question'),
  get_permission('comment_index'),
  get_permission('comment_show'),
  get_permission('comment_create'),
  get_permission('comment_update'),
  get_permission('comment_destroy'),

  create_complex_permission('complex_pertinency_reviewer_permission')
]

style_reviewer_permissions = [
  get_permission('user_password_edit'),
  get_permission('user_update_password'),

  get_permission('question_show'),
  get_permission('question_edit'),

  get_permission('complex_format_reviewer_permission')
]


super_admin = Role.create!(
  name: 'super_admin', 
  permissions: super_admin_permissions, 
  users: [user_admin])

formulador = Role.create!(
  name: 'formulador', 
  permissions: formulador_permissions, 
  users: [user_formulador])

operador = Role.create!(
  name: 'operador', 
  permissions: operador_permissions, 
  users: [user_operador])

revisor_pertinencia = Role.create!(
  name: 'revisor_pertinencia', 
  permissions: revisor_pertinencia_permissions, 
  users: [user_revisor_pertinencia])

revisor_estilo = Role.create!(
  name: 'revisor_estilo', 
  permissions: style_reviewer_permissions, 
  users: [user_revisor_estilo])

# Category.create_or_find_by([{ name: 'Categoria 1' }, { name: 'Categoria 2' }, { name: 'Categoria 3' }, { name: 'Categoria 4' }])

# load the excel file
excel_file = Roo::Spreadsheet.open('lib/Temario__Sw__2_.xlsx')
# navigate to specific sheet
excel_file.sheet(0)
# select row range you wish to iterate over
2.upto(362) do |line|
  first_level = excel_file.cell(line, 'A')
  parent = Category.find_or_create_by(name: first_level, level: 0)

  second_level = excel_file.cell(line, 'B')
  parent = Category.find_or_create_by(name: second_level, level: 1, parent_id: parent.id)

  third_level = excel_file.cell(line, 'C')
  parent = Category.find_or_create_by(name: third_level, level: 2, parent_id: parent.id)

  fourth_level = excel_file.cell(line, 'D')
  separator_1 = fourth_level.split('.')
  separator_1.each do |s1|
    separator_2 = s1.split(';')
    separator_2.each do |s2|
      separator_3 = s2.split('/')
      separator_3.each do |s3|
        Category.find_or_create_by(name: s3, level: 3, parent_id: parent.id, is_leaf: true)
      end
    end
  end

  fourth_level_extra = excel_file.cell(line, 'E')
  next unless fourth_level_extra

  separator_1 = fourth_level_extra.split('.')
  separator_1.each do |s1|
    next unless s1

    separator_2 = s1.split(';')
    separator_2.each do |s2|
      next unless s2

      separator_3 = s2.split('/')
      separator_3.each do |s3|
        Category.find_or_create_by(name: s3, level: 3, parent_id: parent.id, is_leaf: true) if s3
      end
    end
  end  
end
Category.create_or_find_by([{ name: 'Categoria 1' }, { name: 'Categoria 2' }, { name: 'Categoria 3' }, { name: 'Categoria 4' }])
Speciality.create_or_find_by([{ name: 'Especialidad 1' }, { name: 'Especialidad 2' }, { name: 'Especialidad 3' }, { name: 'Especialidad 4' }])

# permissions = Permission.create_or_find_by(
#   [{ name: 'admin_crud' },
#    { name: 'user_view' },
#    { name: 'user_edit' },
#    { name: 'role_crud' },
#    { name: 'role_view' },
#    { name: 'question_create' },
#    { name: 'question_edit' },
#    { name: 'question_show' },
#    { name: 'question_accept' },
#    { name: 'question_edit_image' },
#    { name: 'question_destroy' }]
# )

# permissions = Permission.find_or_create_by(name: 'user_index')
