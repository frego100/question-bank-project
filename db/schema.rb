# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_12_11_052014) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "acceptance_processes", force: :cascade do |t|
    t.string "description"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_acceptance_processes_on_user_id"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "alternatives", force: :cascade do |t|
    t.bigint "question_id"
    t.text "description"
    t.text "formula"
    t.integer "order"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["question_id"], name: "index_alternatives_on_question_id"
  end

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "calls", force: :cascade do |t|
    t.string "name"
    t.integer "status"
    t.text "description"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.string "start_jid"
    t.string "end_jid"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "candidates", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "certificate"
    t.bigint "acceptance_processes_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["acceptance_processes_id"], name: "index_candidates_on_acceptance_processes_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.integer "level"
    t.boolean "is_leaf"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "parent_id"
    t.index ["parent_id"], name: "index_categories_on_parent_id"
  end

  create_table "category_specialities", force: :cascade do |t|
    t.bigint "category_id"
    t.bigint "speciality_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_category_specialities_on_category_id"
    t.index ["speciality_id"], name: "index_category_specialities_on_speciality_id"
  end

  create_table "comments", force: :cascade do |t|
    t.text "comment"
    t.bigint "question_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_id"], name: "index_comments_on_question_id"
  end

  create_table "entrance_exams", force: :cascade do |t|
    t.datetime "defined_at"
    t.datetime "tooked_at"
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "scope"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_permissions_on_name"
  end

  create_table "question_entrance_exams", force: :cascade do |t|
    t.bigint "question_id"
    t.bigint "entrance_exam_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["entrance_exam_id"], name: "index_question_entrance_exams_on_entrance_exam_id"
    t.index ["question_id"], name: "index_question_entrance_exams_on_question_id"
  end

  create_table "question_tracks", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "permission_id"
    t.bigint "question_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["permission_id"], name: "index_question_tracks_on_permission_id"
    t.index ["question_id"], name: "index_question_tracks_on_question_id"
    t.index ["user_id"], name: "index_question_tracks_on_user_id"
  end

  create_table "questions", force: :cascade do |t|
    t.integer "difficulty"
    t.integer "status"
    t.integer "comments_limit", default: 0
    t.integer "revisions_limit", default: 0
    t.text "definition"
    t.text "explanation"
    t.datetime "deleted_at"
    t.datetime "format_approved_at"
    t.datetime "pertinency_approved_at"
    t.bigint "category_id"
    t.bigint "pertinency_reviewer_id"
    t.bigint "format_reviewer_id"
    t.bigint "formulator_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "answer_id"
    t.bigint "area_id"
    t.index ["answer_id"], name: "index_questions_on_answer_id"
    t.index ["area_id"], name: "index_questions_on_area_id"
    t.index ["category_id"], name: "index_questions_on_category_id"
    t.index ["format_reviewer_id"], name: "index_questions_on_format_reviewer_id"
    t.index ["formulator_id"], name: "index_questions_on_formulator_id"
    t.index ["pertinency_reviewer_id"], name: "index_questions_on_pertinency_reviewer_id"
  end

  create_table "requests", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.boolean "aproved"
    t.bigint "calls_id", null: false
    t.bigint "formulator_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["calls_id"], name: "index_requests_on_calls_id"
    t.index ["formulator_id"], name: "index_requests_on_formulator_id"
  end

  create_table "role_permissions", force: :cascade do |t|
    t.bigint "role_id"
    t.bigint "permission_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["permission_id"], name: "index_role_permissions_on_permission_id"
    t.index ["role_id"], name: "index_role_permissions_on_role_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
  end

  create_table "specialities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "taggings", id: :serial, force: :cascade do |t|
    t.integer "tag_id"
    t.string "taggable_type"
    t.integer "taggable_id"
    t.string "tagger_type"
    t.integer "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "taggings_taggable_context_idx"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "user_call_categories", force: :cascade do |t|
    t.bigint "user_call_id", null: false
    t.bigint "category_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_user_call_categories_on_category_id"
    t.index ["user_call_id"], name: "index_user_call_categories_on_user_call_id"
  end

  create_table "user_call_questions", force: :cascade do |t|
    t.bigint "user_call_id", null: false
    t.bigint "question_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_id"], name: "index_user_call_questions_on_question_id"
    t.index ["user_call_id"], name: "index_user_call_questions_on_user_call_id"
  end

  create_table "user_calls", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "call_id", null: false
    t.integer "questions_count"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "questions_limit"
    t.integer "biomedical_questions_limit", default: 0
    t.integer "social_questions_limit", default: 0
    t.integer "engineering_questions_limit", default: 0
    t.integer "biomedical_questions_counter", default: 0
    t.integer "social_questions_counter", default: 0
    t.integer "engineering_questions_counter", default: 0
    t.integer "user_call_questions_count"
    t.index ["call_id"], name: "index_user_calls_on_call_id"
    t.index ["user_id"], name: "index_user_calls_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name", limit: 20
    t.string "last_name", limit: 20
    t.bigint "role_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "deleted_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role_id"], name: "index_users_on_role_id"
  end

  create_table "users_specialities", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "speciality_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["speciality_id"], name: "index_users_specialities_on_speciality_id"
    t.index ["user_id"], name: "index_users_specialities_on_user_id"
  end

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id"
    t.string "foreign_key_name", null: false
    t.integer "foreign_key_id"
    t.string "foreign_type"
    t.index ["foreign_key_name", "foreign_key_id", "foreign_type"], name: "index_version_associations_on_foreign_key"
    t.index ["version_id"], name: "index_version_associations_on_version_id"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.integer "transaction_id"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
    t.index ["transaction_id"], name: "index_versions_on_transaction_id"
  end

  add_foreign_key "acceptance_processes", "users"
  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "alternatives", "questions"
  add_foreign_key "candidates", "acceptance_processes", column: "acceptance_processes_id"
  add_foreign_key "category_specialities", "categories"
  add_foreign_key "category_specialities", "specialities"
  add_foreign_key "comments", "questions"
  add_foreign_key "question_entrance_exams", "entrance_exams"
  add_foreign_key "question_entrance_exams", "questions"
  add_foreign_key "question_tracks", "permissions"
  add_foreign_key "question_tracks", "questions"
  add_foreign_key "question_tracks", "users"
  add_foreign_key "questions", "areas"
  add_foreign_key "questions", "categories"
  add_foreign_key "questions", "users", column: "format_reviewer_id"
  add_foreign_key "questions", "users", column: "formulator_id"
  add_foreign_key "questions", "users", column: "pertinency_reviewer_id"
  add_foreign_key "requests", "calls", column: "calls_id"
  add_foreign_key "requests", "users", column: "formulator_id"
  add_foreign_key "role_permissions", "permissions"
  add_foreign_key "role_permissions", "roles"
  add_foreign_key "taggings", "tags"
  add_foreign_key "user_call_categories", "categories"
  add_foreign_key "user_call_categories", "user_calls"
  add_foreign_key "user_call_questions", "questions"
  add_foreign_key "user_call_questions", "user_calls"
  add_foreign_key "user_calls", "calls"
  add_foreign_key "user_calls", "users"
  add_foreign_key "users", "roles"
  add_foreign_key "users_specialities", "specialities"
  add_foreign_key "users_specialities", "users"

  create_view "user_permissions", sql_definition: <<-SQL
      SELECT u.id AS user_id,
      r.id AS role_id,
      r.name AS role_name,
      rp.role_id AS r_id,
      p.name AS permission_name
     FROM (((users u
       LEFT JOIN roles r ON ((r.id = u.role_id)))
       LEFT JOIN role_permissions rp ON ((r.id = rp.role_id)))
       LEFT JOIN permissions p ON ((rp.permission_id = p.id)));
  SQL
end
