class AddAreaToQuestions < ActiveRecord::Migration[6.0]
  def change
    add_reference :questions, :area, foreign_key: true
  end
end
