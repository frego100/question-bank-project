class CreateUserCallQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :user_call_questions do |t|
      t.references :user_call, null: false, foreign_key: true
      t.references :question, null: false, foreign_key: true

      t.timestamps
    end
  end
end
