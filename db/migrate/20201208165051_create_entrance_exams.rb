class CreateEntranceExams < ActiveRecord::Migration[6.0]
  def change
    create_table :entrance_exams do |t|
      t.datetime :defined_at
      t.datetime :tooked_at
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
