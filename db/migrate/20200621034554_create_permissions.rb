# frozen_string_literal: true

class CreatePermissions < ActiveRecord::Migration[6.0]
  def change
    create_table :permissions do |t|
      t.string :name, index: true, unique: true
      t.text :description
      t.integer :scope

      t.timestamps
    end
  end
end
