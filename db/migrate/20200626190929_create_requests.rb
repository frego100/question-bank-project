# frozen_string_literal: true

class CreateRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :requests do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.boolean :aproved
      t.references :calls, foreign_key: true, null: false
      t.references :formulator, foreign_key: { to_table: :users }

      t.timestamps
    end
  end
end
