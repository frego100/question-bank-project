class AddColumnsToUserCalls < ActiveRecord::Migration[6.0]
  def change
    add_column :user_calls, :biomedical_questions_limit, :integer, default: 0
    add_column :user_calls, :social_questions_limit, :integer, default: 0
    add_column :user_calls, :engineering_questions_limit, :integer, default: 0
    add_column :user_calls, :biomedical_questions_counter, :integer, default: 0
    add_column :user_calls, :social_questions_counter, :integer, default: 0
    add_column :user_calls, :engineering_questions_counter, :integer, default: 0
  end
end
