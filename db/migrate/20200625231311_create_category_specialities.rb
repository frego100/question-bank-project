# frozen_string_literal: true

class CreateCategorySpecialities < ActiveRecord::Migration[6.0]
  def change
    create_table :category_specialities do |t|
      t.references :category, foreign_key: true
      t.references :speciality, foreign_key: true

      t.timestamps
    end
  end
end
