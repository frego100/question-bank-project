# frozen_string_literal: true

class CreateUserCalls < ActiveRecord::Migration[6.0]
  def change
    create_table :user_calls do |t|
      t.references :user, foreign_key: true, null: false
      t.references :call, foreign_key: true, null: false
      t.integer :questions_count

      t.timestamps
    end
  end
end
