class AddQuestionsLimitToUserCall < ActiveRecord::Migration[6.0]
  def change
    add_column :user_calls, :questions_limit, :int, null: true
  end
end
