# frozen_string_literal: true

class CreateUsersSpecialities < ActiveRecord::Migration[6.0]
  def change
    create_table :users_specialities do |t|
      t.references :user, foreign_key: true
      t.references :speciality, foreign_key: true

      t.timestamps
    end
  end
end
