# frozen_string_literal: true

class CreateCandidates < ActiveRecord::Migration[6.0]
  def change
    create_table :candidates do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :certificate
      t.references :acceptance_processes, foreign_key: true, null: false

      t.timestamps
    end
  end
end
