# frozen_string_literal: true

class CreateAcceptanceProcesses < ActiveRecord::Migration[6.0]
  def change
    create_table :acceptance_processes do |t|
      t.string :description
      t.timestamp :starts_at
      t.timestamp :ends_at
      t.references :user, foreign_key: true, null: false

      t.timestamps
    end
  end
end
