class CreateQuestionTracks < ActiveRecord::Migration[6.0]
  def change
    create_table :question_tracks do |t|
      t.references :user, foreign_key: true
      t.references :permission, foreign_key: true
      t.references :question, foreign_key: true
      t.timestamps
    end
  end
end
