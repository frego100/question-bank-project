# frozen_string_literal: true

class AddCategorySelfReference < ActiveRecord::Migration[6.0]
  def change
    change_table :categories do |t|
      t.references :parent
    end
  end
end
