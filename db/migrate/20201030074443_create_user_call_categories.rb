class CreateUserCallCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :user_call_categories do |t|
      t.references :user_call, foreign_key: true, null: false
      t.references :category, foreign_key: true, null: false

      t.timestamps
    end
  end
end
