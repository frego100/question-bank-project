class CreateQuestionEntranceExams < ActiveRecord::Migration[6.0]
  def change
    create_table :question_entrance_exams do |t|
      t.references :question, foreign_key: true
      t.references :entrance_exam, foreign_key: true

      t.timestamps
    end
  end
end
