class AddDeletedAtToAlternative < ActiveRecord::Migration[6.0]
  def change
    add_column :alternatives, :deleted_at, :datetime
  end
end
