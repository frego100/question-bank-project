class AddCounterToUserCalls < ActiveRecord::Migration[6.0]
  def change
    add_column :user_calls, :user_call_questions_count, :integer
  end
end
