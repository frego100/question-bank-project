# frozen_string_literal: true

class CreateCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :level
      t.boolean :is_leaf

      t.timestamps
    end
  end
end
