# frozen_string_literal: true

class CreateCalls < ActiveRecord::Migration[6.0]
  def change
    create_table :calls do |t|
      t.string :name
      t.integer :status
      t.text :description
      t.timestamp :starts_at
      t.timestamp :ends_at
      t.string :start_jid
      t.string :end_jid
      t.timestamp :started_at
      t.timestamp :ended_at

      t.timestamps
    end
  end
end
