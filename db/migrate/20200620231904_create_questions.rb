# frozen_string_literal: true

class CreateQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :questions do |t|
      t.integer :difficulty
      t.integer :status
      t.integer :comments_limit, default: 0
      t.integer :revisions_limit, default: 0
      t.text :definition
      t.text :explanation
      t.timestamp :deleted_at
      t.timestamp :format_approved_at
      t.timestamp :pertinency_approved_at
      t.references :category, foreign_key: true
      t.references :pertinency_reviewer, foreign_key: { to_table: :users }
      t.references :format_reviewer, foreign_key: { to_table: :users }
      t.references :formulator, foreign_key: { to_table: :users }

      t.timestamps
    end
  end
end
