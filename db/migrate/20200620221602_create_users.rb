# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :first_name, limit: 20
      t.string :last_name, limit: 20
      t.references :role, foreign_key: true

      t.timestamps
    end
  end
end
