# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :alternative do
    description { "<p>#{Faker::Lorem.paragraph_by_chars}</p>" }
  end
end
