# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :role do
    factory :formulator_role do
      name { 'formulador' }
    end
    factory :operator_role do
      name { 'operador' }
    end
    factory :pertinency_role do
      name { 'pertinencia' }
    end
    factory :style_role do
      name { 'estilo' }
    end
    factory :admin_role do
      name { 'administrator' }
    end
  end
end

def create_formulator_role
  role = FactoryBot.create(:formulator_role)
  permission = Permission.where(name: 'complex_formulator_permission').first
  FactoryBot.create(:role_permission, role_id: role.id, permission_id: permission.id)
  role
end

def create_operator_role
  role = FactoryBot.create(:operator_role)
  permissions = Permission.where(name: %w[complex_operator_permission question_show])
  permissions.each do |p|
    FactoryBot.create(:role_permission, role_id: role.id, permission_id: p.id)
  end
  role
end

def create_pertinency_role
  role = FactoryBot.create(:pertinency_role)
  permissions = Permission.where(name: %w[complex_pertinency_reviewer_permission question_show question_approve_question question_reject_question])
  permissions.each do |p|
    FactoryBot.create(:role_permission, role_id: role.id, permission_id: p.id)
  end
  role
end

def create_style_role
  role = FactoryBot.create(:style_role)
  permissions = Permission.where(name: %w[complex_format_reviewer_permission question_show question_edit])
  permissions.each do |p|
    FactoryBot.create(:role_permission, role_id: role.id, permission_id: p.id)
  end
  role
end

def create_admin_role
  role = FactoryBot.create(:admin_role)
  permissions = Permission.all
  permissions.each do |p|
    FactoryBot.create(:role_permission, role_id: role.id, permission_id: p.id)
  end
  role
end
