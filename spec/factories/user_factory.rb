# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    password { Faker::Name.first_name + 'Algo1*' }
    email { Faker::Name.first_name + 'algo@unsa.edu.pe' }

    factory :formulator do
      email { 'formulator@unsa.edu.pe' }
    end
    factory :operator do
      email { 'operator@unsa.edu.pe' }
    end
    factory :pertinency do
      email { 'pertinency@unsa.edu.pe' }
    end
    factory :style do
      email { 'estilo@unsa.edu.pe' }
    end
    factory :admin do
      email { 'bueno@unsa.edu.pe' }
    end
  end
end
