# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :call do
    name { Faker::Company.buzzword }
    starts_at { '2020-12-01 00:00:00' }
    ends_at { '2020-12-31 00:00:00' }
    status { 'inactive' }

    factory :active_call do
      status { 'active' }
    end
  end
end

def create_active_call(user:)
  active_call = FactoryBot.create(:active_call)
  FactoryBot.create(:user_call, user_id: user.id, call_id: active_call.id)
  active_call
end
