# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :category do
    name { Faker::Company.buzzword }
    level { 3 }

    factory :leaf_category do
      is_leaf { true }
    end
  end
end
