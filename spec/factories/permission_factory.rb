# frozen_string_literal: true

require 'faker'
require 'pp'

FactoryBot.define do
  factory :permission do
    description { Faker::Lorem.paragraph }
    scope { 1 }
  end
end

def set_all_permissions
  permissions = %w[user_index user_show user_edit user_new user_create user_update user_destroy
                   user_password_edit user_update_password user_pertinency_reviewers user_format_reviewers
                   role_index role_show role_edit role_new role_create role_update role_destroy question_index
                   question_index_operator question_index_formulator question_index_reviewer question_show
                   question_edit question_update question_new question_create question_destroy
                   question_assign_question question_approve_question question_reject_question
                   question_send_question_to_review question_observe_question comment_index
                   comment_show comment_create comment_update comment_destroy call_index call_show
                   call_edit call_new call_create call_update call_destroy call_add_formulator
                   call_switch_status category_list category_edit_tree category_index
                   category_create category_update category_destroy complex_formulator_permission
                   complex_pertinency_reviewer_permission complex_format_reviewer_permission
                   complex_operator_permission]

  FactoryBot.build_list(:permission, permissions.size) do |permission, i|
    permission.name = permissions[i]
    permission.save
  end
end
