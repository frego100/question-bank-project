# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :question do
    comments_limit { 0 }
    difficulty { :low }
    revisions_limit { 0 }

    factory :incomplete_question do
      status { :incomplete }
      definition { "<p>#{Faker::Lorem.paragraph_by_chars}</p>" }
      explanation { "<p>#{Faker::Lorem.paragraph_by_chars}</p>" }
    end
  end
end

def question_with_alternatives(alternative_count: 5, formulator:, category:)
  FactoryBot.create(:incomplete_question, formulator: formulator, category: category) do |iq|
    FactoryBot.build_list(:alternative, alternative_count) do |alternative, i|
      alternative.order = i
      alternative.question = iq
      alternative.save
      if i == 0
        iq.answer_id = alternative.id
        iq.save
      end
    end
  end
end
