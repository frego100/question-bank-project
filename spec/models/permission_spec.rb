# frozen_string_literal: true

require 'rails_helper'

describe Permission do
  before(:all) do
    @permission = create(:permission)
  end

  it 'is valid with valid attributes' do
    expect(@permission).to be_valid
  end

  it 'has unique name' do
    permission2 = build(:permission, name: @permission.name)
    expect(permission2).to_not be_valid
  end

  it 'is not valid without name' do
    permission2 = build(:permission, name: nil)
    expect(permission2).to_not be_valid
  end
end
