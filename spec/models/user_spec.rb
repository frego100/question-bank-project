# frozen_string_literal: true

require 'rails_helper'

describe User do
  before(:all) do
    @user = create(:user)
  end

  # first_name
  it 'is valid with valid attributes' do
    expect(@user).to be_valid
  end

  it 'has not unique name' do
    user2 = build(:user, first_name: @user.first_name)
    expect(user2).to be_valid
  end

  it 'is not valid without name' do
    user2 = build(:user, first_name: nil)
    expect(user2).to_not be_valid
  end

  it 'is not valid if name is a number' do
    user2 = build(:user, first_name: (begin
                                  !Float(@user.first_name).nil?
                                      rescue StandardError
                                        false
                                end))
    expect(user2).to be_valid
  end

  it 'it is too long' do
    user2 = build(:user, first_name: @user.first_name.length < 100)
    expect(user2).to be_valid
  end

  # last_name
  it 'has not unique last_name' do
    user2 = build(:user, last_name: @user.last_name)
    expect(user2).to be_valid
  end

  it 'is not valid without last_name' do
    user2 = build(:user, last_name: nil)
    expect(user2).to_not be_valid
  end

  it 'is not valid if last_name is a number' do
    user2 = build(:user, last_name: (begin
                                  !Float(@user.last_name).nil?
                                     rescue StandardError
                                       false
                                end))
    expect(user2).to be_valid
  end

  it 'it is too long' do
    user2 = build(:user, last_name: @user.last_name.length < 100)
    expect(user2).to be_valid
  end

  # role is already validate in its spec
  it 'has not role' do
    user2 = build(:user, role_id: nil)
    expect(user2).to_not be_valid
  end

  # email
  it 'has not email' do
    user2 = build(:user, email: nil)
    expect(user2).to_not be_valid
  end

  it 'it ends with @unsa.edu.pe' do
    user2 = build(:user, email: (@user.email.end_with? '@unsa.edu.pe'))
    expect(user2).to_not be_valid
  end

  # password
  it 'has not password' do
    user2 = build(:user, password: nil)
    expect(user2).to_not be_valid
  end

  it 'its too short' do
    user2 = build(:user, password: @user.password.length < 7)
    expect(user2).to_not be_valid
  end

  it 'its too long' do
    user2 = build(:user, password: @user.password.length > 100)
    expect(user2).to_not be_valid
  end

  it 'it is not secure' do
    user2 = build(:user, password: @user.password.match(/abc/))
    expect(user2).to_not be_valid
  end
end
