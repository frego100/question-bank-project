# frozen_string_literal: true

require 'rails_helper'

describe Role do
  before(:all) do
    @role = create(:role)
  end

  it 'is valid with valid attributes' do
    expect(@role).to be_valid
  end

  it 'has unique name' do
    role2 = build(:role, name: @role.name)
    expect(role2).to_not be_valid
  end

  it 'is not valid without name' do
    role2 = build(:role, name: nil)
    expect(role2).to_not be_valid
  end

  it 'is not valid if name is a number' do
    role2 = build(:role, name: (begin
                                  !Float(@role.name).nil?
                                rescue StandardError
                                  false
                                end))
    expect(role2).to be_valid
  end

  it 'its has appropriate length' do
    role2 = build(:role, name: @role.name.length < 100)
    expect(role2).to be_valid
  end
end
