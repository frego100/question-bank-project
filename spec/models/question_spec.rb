# frozen_string_literal: true

require 'rails_helper'

describe Question do
  before(:all) do
    # @permission = create(:permission)
    set_all_permissions
  end

  it 'IMPT-00006 ELI-015 El sistema crea la pregunta en base al id del formulador' do
    formulator_role = create_formulator_role
    formulator = build(:formulator)
    formulator.role = formulator_role
    formulator.save
    category = create(:leaf_category)

    question = question_with_alternatives(formulator: formulator, category: category)
    operation = question.save

    result = operation && (formulator.id == question.formulator_id)

    expect(result).to be true
  end

  # it 'has unique name' do
  #   permission2 = build(:permission, name: @permission.name)
  #   expect(permission2).to_not be_valid
  # end

  # it 'is not valid without name' do
  #   permission2 = build(:permission, name: nil)
  #   expect(permission2).to_not be_valid
  # end
end
