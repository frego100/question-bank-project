# frozen_string_literal: true

require 'rails_helper'

describe Question, type: :request do
  before :each do
    set_all_permissions
  end

  it 'ELI-068 rigth way to create question' do
    admin_role = create_admin_role
    admin = create(:admin, role: admin_role)
    create_active_call(user: admin)
    sign_in(admin)

    category = create(:leaf_category)

    get '/questions/new'
    expect(response).to render_template(:new)

    post '/questions',
         params: {
           question: {
             tag_list: 'historia',
             category_id: category.id,
             difficulty: 'low',
             definition: '¿Cuándo fue la primer guerra mundial?',
             explanation: 'En 1914',
             alt_a: '1911',
             alt_b: '1912',
             alt_c: '1913',
             alt_d: '1914',
             alt_e: '1915'
           }
         }
    follow_redirect!

    expect(response.body).to include('La pregunta se creó correctamente.')
  end

  it 'ELI-069 create question without permissions' do
    # for example, create an operator user, he doesn't have permission to create a question
    operator_role = create_operator_role
    operator = create(:operator, role: operator_role)
    create_active_call(user: operator)
    sign_in(operator)

    get '/questions/new'

    expect(response.body).to include('404')
  end

  it 'ELI-070 create question without active session' do
    formulator_role = create_formulator_role
    formulator = create(:formulator, role: formulator_role)
    create_active_call(user: formulator)
    category = create(:leaf_category)

    post '/questions',
         params: {
           question: {
             tag_list: 'historia',
             category_id: category.id,
             difficulty: 'low',
             definition: '¿Cuándo fue la primer guerra mundial?',
             explanation: 'En 1914',
             alt_a: '1911',
             alt_b: '1912',
             alt_c: '1913',
             alt_d: '1914',
             alt_e: '1915'
           }
         }

    follow_redirect!
    expect(response.body).to include('You need to sign in or sign up before continuing.')
  end

  it 'ELI-071  formulator saves a question' do
    formulator_role = create_formulator_role
    formulator = create(:formulator, role: formulator_role)
    create_active_call(user: formulator)
    sign_in(formulator)

    category = create(:leaf_category)

    get '/questions/new'
    expect(response).to render_template(:new)

    post '/questions',
         params: {
           question: {
             tag_list: 'historia',
             category_id: category.id,
             difficulty: 'low',
             definition: '¿Cuándo fue la primer guerra mundial?',
             explanation: 'En 1914',
             alt_a: '1911',
             alt_b: '1912',
             alt_c: '1913',
             alt_d: '1914',
             alt_e: '1915'
           }
         }
    follow_redirect!

    expect(response.body).to include('La pregunta se creó correctamente.')
  end

  it 'ELI-072 formulator saves a question without category' do
    formulator_role = create_formulator_role
    formulator = create(:formulator, role: formulator_role)
    create_active_call(user: formulator)
    sign_in(formulator)

    get '/questions/new'
    expect(response).to render_template(:new)

    post '/questions',
         params: {
           question: {
             tag_list: 'historia',
             difficulty: 'low',
             definition: '¿Cuándo fue la primer guerra mundial?',
             explanation: 'En 1914',
             alt_a: '1911',
             alt_b: '1912',
             alt_c: '1913',
             alt_d: '1914',
             alt_e: '1915'
           }
         }

    expect(response.body).to include('prohibited this question from being saved')
  end

  # Here test ELI-73

  it 'ELI-074 formulator saves a question without content' do
    formulator_role = create_formulator_role
    formulator = create(:formulator, role: formulator_role)
    create_active_call(user: formulator)
    sign_in(formulator)

    category = create(:leaf_category)

    get '/questions/new'
    expect(response).to render_template(:new)

    post '/questions',
         params: {
           question: {
             tag_list: 'historia',
             category_id: category.id,
             difficulty: 'low',
             # without definition: '¿Cuándo fue la primer guerra mundial?',
             explanation: 'En 1914',
             alt_a: '1911',
             alt_b: '1912',
             alt_c: '1913',
             alt_d: '1914',
             alt_e: '1915'
           }
         }
    follow_redirect!

    expect(response.body).to_not include('La pregunta se creó correctamente.')
    # expect(response.body).to_not include('prohibited this question from being saved')
  end

  # Here test ELI-75
  # Here test ELI-76
end
