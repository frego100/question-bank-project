# frozen_string_literal: true

desc 'This task is called in order to prepare the data inside the redis db'
task connect_to_redis: :environment do
  RedisJob.perform_now
end
