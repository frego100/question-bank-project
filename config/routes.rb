# frozen_string_literal: true

require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/999000bank000999'
  # resources :requests
  resources :calls do
    post :add_formulator, on: :member
    post :add_pertinency_reviewer, on: :member
    post :add_format_reviewer, on: :member
    post :switch_status, on: :member
    post 'edit_formulator_categories/:user_call_id', to: 'calls#edit_formulator_categories', as: 'edit_formulator_categories'
  end
  # resources :candidates
  resources :acceptance_processes
  # resources :category_specialities
  # resources :alternatives
  get 'categories/list', action: :list, controller: 'categories'
  get 'categories/edit_tree', action: :edit_tree, controller: 'categories'
  resources :categories, except: %i[edit show new]
  # resources :users_specialities
  devise_for :users, controllers: { sessions: 'users/sessions', passwords: 'users/passwords' }

  # resources :question_tracks
  # resources :specialities
  # resources :role_permissions
  # resources :permissions
  resources :entrance_exams do
    post 'add_question/:question_id', to: 'entrance_exams#add_question', as: 'add_question_to_entrance_exam'
    delete 'remove_question/:question_id', to: 'entrance_exams#remove_question', as: 'remove_question_from_entrance_exam'
  end

  get 'tags/:tag', to: 'questions#index_formulator', as: :tag
  get '/questions/index_operator', to: 'questions#index_operator'
  get '/questions/index_formulator', to: 'questions#index_formulator'
  get '/questions/index_reviewer', to: 'questions#index_reviewer'
  get '/questions/edit_formulator', to: 'questions#edit_formulator', as: 'question_formulator'
  get '/questions/edit_reviewer', to: 'questions#edit_reviewer', as: 'edit_reviewer'
  get '/questions/formulator_status', to: 'questions#formulator_status', as: 'formulator_status'
  get '/questions/reviewer_status', to: 'questions#reviewer_status', as: 'reviewer_status'
  resources :questions do
    resources :comments, only: %i[index show create update destroy]
    post :assign_question, on: :member
    post :approve_question, on: :member
    post :reject_question, on: :member
    post :send_question_to_review, on: :member
    post :observe_question, on: :member
  end
  get '/users/pertinency_reviewers', to: 'users#pertinency_reviewers'
  get '/users/format_reviewers', to: 'users#format_reviewers'
  resources :users do
    patch :update_password, on: :member
    get :password_edit, on: :member
  end
  resources :roles
  root to: 'welcome#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
