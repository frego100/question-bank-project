# Preview all emails at http://localhost:3000/rails/mailers/first_mailer
class FirstMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/first_mailer/first_mail
  def first_mail
    FirstMailer.first_mail
  end

end
