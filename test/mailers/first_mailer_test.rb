require 'test_helper'

class FirstMailerTest < ActionMailer::TestCase
  test "first_mail" do
    mail = FirstMailer.first_mail
    assert_equal "First mail", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
