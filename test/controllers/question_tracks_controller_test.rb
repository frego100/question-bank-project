require 'test_helper'

class QuestionTracksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @question_track = question_tracks(:one)
  end

  test "should get index" do
    get question_tracks_url
    assert_response :success
  end

  test "should get new" do
    get new_question_track_url
    assert_response :success
  end

  test "should create question_track" do
    assert_difference('QuestionTrack.count') do
      post question_tracks_url, params: { question_track: {  } }
    end

    assert_redirected_to question_track_url(QuestionTrack.last)
  end

  test "should show question_track" do
    get question_track_url(@question_track)
    assert_response :success
  end

  test "should get edit" do
    get edit_question_track_url(@question_track)
    assert_response :success
  end

  test "should update question_track" do
    patch question_track_url(@question_track), params: { question_track: {  } }
    assert_redirected_to question_track_url(@question_track)
  end

  test "should destroy question_track" do
    assert_difference('QuestionTrack.count', -1) do
      delete question_track_url(@question_track)
    end

    assert_redirected_to question_tracks_url
  end
end
