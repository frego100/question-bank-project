require 'test_helper'

class CategorySpecialitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @category_speciality = category_specialities(:one)
  end

  test "should get index" do
    get category_specialities_url
    assert_response :success
  end

  test "should get new" do
    get new_category_speciality_url
    assert_response :success
  end

  test "should create category_speciality" do
    assert_difference('CategorySpeciality.count') do
      post category_specialities_url, params: { category_speciality: {  } }
    end

    assert_redirected_to category_speciality_url(CategorySpeciality.last)
  end

  test "should show category_speciality" do
    get category_speciality_url(@category_speciality)
    assert_response :success
  end

  test "should get edit" do
    get edit_category_speciality_url(@category_speciality)
    assert_response :success
  end

  test "should update category_speciality" do
    patch category_speciality_url(@category_speciality), params: { category_speciality: {  } }
    assert_redirected_to category_speciality_url(@category_speciality)
  end

  test "should destroy category_speciality" do
    assert_difference('CategorySpeciality.count', -1) do
      delete category_speciality_url(@category_speciality)
    end

    assert_redirected_to category_specialities_url
  end
end
