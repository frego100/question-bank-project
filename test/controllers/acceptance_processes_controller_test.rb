require 'test_helper'

class AcceptanceProcessesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @acceptance_process = acceptance_processes(:one)
  end

  test "should get index" do
    get acceptance_processes_url
    assert_response :success
  end

  test "should get new" do
    get new_acceptance_process_url
    assert_response :success
  end

  test "should create acceptance_process" do
    assert_difference('AcceptanceProcess.count') do
      post acceptance_processes_url, params: { acceptance_process: { description: @acceptance_process.description } }
    end

    assert_redirected_to acceptance_process_url(AcceptanceProcess.last)
  end

  test "should show acceptance_process" do
    get acceptance_process_url(@acceptance_process)
    assert_response :success
  end

  test "should get edit" do
    get edit_acceptance_process_url(@acceptance_process)
    assert_response :success
  end

  test "should update acceptance_process" do
    patch acceptance_process_url(@acceptance_process), params: { acceptance_process: { description: @acceptance_process.description } }
    assert_redirected_to acceptance_process_url(@acceptance_process)
  end

  test "should destroy acceptance_process" do
    assert_difference('AcceptanceProcess.count', -1) do
      delete acceptance_process_url(@acceptance_process)
    end

    assert_redirected_to acceptance_processes_url
  end
end
