require "application_system_test_case"

class CategorySpecialitiesTest < ApplicationSystemTestCase
  setup do
    @category_speciality = category_specialities(:one)
  end

  test "visiting the index" do
    visit category_specialities_url
    assert_selector "h1", text: "Category Specialities"
  end

  test "creating a Category speciality" do
    visit category_specialities_url
    click_on "New Category Speciality"

    click_on "Create Category speciality"

    assert_text "Category speciality was successfully created"
    click_on "Back"
  end

  test "updating a Category speciality" do
    visit category_specialities_url
    click_on "Edit", match: :first

    click_on "Update Category speciality"

    assert_text "Category speciality was successfully updated"
    click_on "Back"
  end

  test "destroying a Category speciality" do
    visit category_specialities_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Category speciality was successfully destroyed"
  end
end
