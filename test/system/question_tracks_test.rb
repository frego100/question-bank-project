require "application_system_test_case"

class QuestionTracksTest < ApplicationSystemTestCase
  setup do
    @question_track = question_tracks(:one)
  end

  test "visiting the index" do
    visit question_tracks_url
    assert_selector "h1", text: "Question Tracks"
  end

  test "creating a Question track" do
    visit question_tracks_url
    click_on "New Question Track"

    click_on "Create Question track"

    assert_text "Question track was successfully created"
    click_on "Back"
  end

  test "updating a Question track" do
    visit question_tracks_url
    click_on "Edit", match: :first

    click_on "Update Question track"

    assert_text "Question track was successfully updated"
    click_on "Back"
  end

  test "destroying a Question track" do
    visit question_tracks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Question track was successfully destroyed"
  end
end
