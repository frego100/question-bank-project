require "application_system_test_case"

class AcceptanceProcessesTest < ApplicationSystemTestCase
  setup do
    @acceptance_process = acceptance_processes(:one)
  end

  test "visiting the index" do
    visit acceptance_processes_url
    assert_selector "h1", text: "Acceptance Processes"
  end

  test "creating a Acceptance process" do
    visit acceptance_processes_url
    click_on "New Acceptance Process"

    fill_in "Description", with: @acceptance_process.description
    click_on "Create Acceptance process"

    assert_text "Acceptance process was successfully created"
    click_on "Back"
  end

  test "updating a Acceptance process" do
    visit acceptance_processes_url
    click_on "Edit", match: :first

    fill_in "Description", with: @acceptance_process.description
    click_on "Update Acceptance process"

    assert_text "Acceptance process was successfully updated"
    click_on "Back"
  end

  test "destroying a Acceptance process" do
    visit acceptance_processes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Acceptance process was successfully destroyed"
  end
end
