# frozen_string_literal: true

class RedisJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    redis = Redis.new(db: 0)
    redis.ping
    redis.del 'is_formulation_process_active'
    redis.del 'formulation_process_formulators'
    redis.del 'formulation_process_pertinency_reviewers'
    redis.del 'formulation_process_format_reviewers'
    active_calls = Call.where(status: 'active')
    active_call = active_calls.first
    if active_call
      redis.set 'is_formulation_process_active', true
      formulator_ids = UserPermission.where(permission_name: 'complex_formulator_permission').pluck(:user_id)
      pertinency_reviewer_ids = UserPermission.where(permission_name: 'complex_pertinency_reviewer_permission').pluck(:user_id)
      format_reviewer_ids = UserPermission.where(permission_name: 'complex_format_reviewer_permission').pluck(:user_id)  
      current_formulators = active_call.user_calls.where(user_id: formulator_ids).pluck(:user_id)
      current_pertinency_reviewers = active_call.user_calls.where(user_id: pertinency_reviewer_ids).pluck(:user_id)
      current_format_reviewers = active_call.user_calls.where(user_id: format_reviewer_ids).pluck(:user_id)
      redis.rpush 'formulation_process_formulators', current_formulators unless current_formulators.empty?
      redis.rpush 'formulation_process_pertinency_reviewers', current_pertinency_reviewers unless current_pertinency_reviewers.empty?
      redis.rpush 'formulation_process_format_reviewers', current_format_reviewers unless current_format_reviewers.empty?
    end
  rescue RuntimeError
    format.html { redirect_to root_path, alert: 'Something went wrong.' }
  end
end
