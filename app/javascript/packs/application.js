// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start();
require("turbolinks").start();
require("@rails/activestorage").start();
require("channels");
import $ from "jquery";
require("bootstrap");
require("./stylesheets/application.scss");
require("select2");
import "select2/dist/css/select2.css";
import "select2-bootstrap-theme/dist/select2-bootstrap.css";

require("datatables.net");
require("datatables.net-dt/css/jquery.dataTables.css");

require("@ckeditor/ckeditor5-build-inline");
require("@wiris/mathtype-generic");
require("jquery-validation");

import "jquery.fancytree/dist/skin-lion/ui.fancytree.css";

$(document).on("turbolinks:load", function () {
  $("#question-form").validate({
    rules: {
      "question[alt_a]": "required",
      "question[alt_b]": "required",
      "question[alt_c]": "required",
      "question[alt_d]": "required",
      "question[alt_e]": "required",
      "question[tag_list]": "required",
    },
    messages: {
      "question[alt_a]": "Este Campo es obligatorio",
      "question[alt_b]": "Este Campo es obligatorio",
      "question[alt_c]": "Este Campo es obligatorio",
      "question[alt_d]": "Este Campo es obligatorio",
      "question[alt_e]": "Este Campo es obligatorio",
      "question[tag_list]": "Este Campo es obligatorio",
    },
  });
});

$(document).on("turbolinks:before-cache", function () {
  $("#role_permission_ids").select2("destroy");
  $("#user_role_id").select2("destroy");
  $("#call_formulator_id").select2("destroy");
  $("#call_pertinency_reviewer_id").select2("destroy");
  $("#call_format_reviewer_id").select2("destroy");
  $("#call_category_ids").select2("destroy");
  $("#user_speciality_ids").select2("destroy");
});

$(document).on("turbolinks:load", function () {
  $("#modalEditCategoriesForm").on("show.bs.modal", function (e) {
    console.log(e.relatedTarget.dataset);
    $("#user-name").html(e.relatedTarget.dataset.name);
    $("#user_call_category_ids").select2({
      allowClear: false,
      theme: "bootstrap",
      width: "100%",
      placeholder: "Seleccionar Categorias",
      class: "form-control",
      multiple: true,
    });
    $("#user_call_category_ids").val(
      e.relatedTarget.dataset.categories.split(",")
    );
    $("#user_call_category_ids").trigger("change");
    $(
      "#user_call_form"
    )[0].action = `${e.relatedTarget.dataset.callId}/edit_formulator_categories/${e.relatedTarget.dataset.userCallId}`;
  });

  $("#modalEditCategoriesForm").on("hidden.bs.modal", function (e) {
    $("#user-name").html("");
    $("#user_call_category_ids").select2("destroy");
    $(
      "#user_call_form"
    )[0].action = `${e.relatedTarget.dataset.call_id}/edit_formulator_categories/`;
  });

  $("#role_permission_ids").select2({
    theme: "bootstrap",
    width: "100%",
    placeholder: "Seleccionar permisos",
    multiple: true,
    class: "form-control",
  });

  $("#user_role_id").select2({
    allowClear: false,
    theme: "bootstrap",
    width: "300px",
    placeholder: "Seleccionar rol",
    class: "form-control",
  });

  $("#call_formulator_id").select2({
    allowClear: false,
    theme: "bootstrap",
    width: "300px",
    placeholder: "Seleccionar Formulador",
    class: "form-control",
  });

  $("#call_pertinency_reviewer_id").select2({
    allowClear: false,
    theme: "bootstrap",
    width: "300px",
    placeholder: "Seleccionar Formulador",
    class: "form-control",
  });

  $("#call_format_reviewer_id").select2({
    allowClear: false,
    theme: "bootstrap",
    width: "300px",
    placeholder: "Seleccionar Formulador",
    class: "form-control",
  });

  $("#call_category_ids").select2({
    allowClear: false,
    theme: "bootstrap",
    width: "300px",
    placeholder: "Seleccionar Categorias",
    class: "form-control",
    multiple: true,
  });

  $("#user_speciality_ids").select2({
    allowClear: false,
    theme: "bootstrap",
    width: "300px",
    placeholder: "Seleccionar Especialidad",
    class: "form-control",
    multiple: true,
  });

  $("#question_category_id").select2({
    allowClear: false,
    theme: "bootstrap",
    width: "300px",
    placeholder: "Seleccionar Especialidad",
    class: "form-control",
    multiple: false,
  });

  $("#question_difficulty").select2({
    allowClear: false,
    theme: "bootstrap",
    width: "300px",
    placeholder: "Seleccionar Especialidad",
    class: "form-control",
    multiple: false,
  });

  $("#question_area_id").select2({
    allowClear: false,
    theme: "bootstrap",
    width: "300px",
    placeholder: "Seleccionar Especialidad",
    class: "form-control",
    multiple: false,
  });

  let question_editor;
  let explanation_editor;
  let editor_alternative_a;
  let editor_alternative_b;
  let editor_alternative_c;
  let editor_alternative_d;
  let editor_alternative_e;

  let question_areas = document.querySelector("#questiondefinition");
  let explanation_areas = document.querySelector("#questionexplanation");

  let question_areas_disabled = document.querySelector("#question_disabled");
  let explanation_areas_disabled = document.querySelector(
    "#explanation_disabled"
  );

  let alternative_a = document.querySelector("#alt_a");
  let alternative_b = document.querySelector("#alt_b");
  let alternative_c = document.querySelector("#alt_c");
  let alternative_d = document.querySelector("#alt_d");
  let alternative_e = document.querySelector("#alt_e");

  if (question_areas) {
    var editor_question_definition = InlineEditor.create(question_areas, {
      toolbar: [
        "heading",
        "bold",
        "italic",
        "link",
        "bulletedList",
        "numberedList",
        "-",
        "indent",
        "outdent",
        "blockQuote",
        "insertTable",
        "undo",
        "redo",
      ],
    })
      .then((newEditor) => {
        question_editor = newEditor;
        console.log(Array.from(newEditor.ui.componentFactory.names()));
      })
      .catch((error) => {
        console.error(error);
      });
  }

  if (explanation_areas) {
    InlineEditor.create(explanation_areas, {
      toolbar: [
        "heading",
        "bold",
        "italic",
        "link",
        "bulletedList",
        "numberedList",
        "-",
        "indent",
        "outdent",
        "blockQuote",
        "insertTable",
        "undo",
        "redo",
      ],
    })
      .then((newEditor) => {
        explanation_editor = newEditor;
      })
      .catch((error) => {
        console.error(error);
      });
  }

  if (question_areas_disabled) {
    InlineEditor.create(question_areas_disabled)
      .then((editor) => {
        console.log(editor);
        editor.isReadOnly = true; // make the editor read-only right after initialization
      })
      .catch((error) => {
        console.error(error);
      });
  }

  if (explanation_areas_disabled) {
    InlineEditor.create(explanation_areas_disabled)
      .then((editor) => {
        console.log(editor);
        editor.isReadOnly = true; // make the editor read-only right after initialization
      })
      .catch((error) => {
        console.error(error);
      });
  }

  if (alternative_a) {
    InlineEditor.create(alternative_a, {
      toolbar: [
        "heading",
        "bold",
        "italic",
        "link",
        "bulletedList",
        "numberedList",
        "-",
        "indent",
        "outdent",
        "blockQuote",
        "insertTable",
        "undo",
        "redo",
      ],
    })
      .then((newEditor) => {
        editor_alternative_a = newEditor;
      })
      .catch((error) => {
        console.error(error);
      });
  }
  if (alternative_b) {
    InlineEditor.create(alternative_b, {
      toolbar: [
        "heading",
        "bold",
        "italic",
        "link",
        "bulletedList",
        "numberedList",
        "-",
        "indent",
        "outdent",
        "blockQuote",
        "insertTable",
        "undo",
        "redo",
      ],
    })
      .then((newEditor) => {
        editor_alternative_b = newEditor;
      })
      .catch((error) => {
        console.error(error);
      });
  }
  if (alternative_c) {
    InlineEditor.create(alternative_c, {
      toolbar: [
        "heading",
        "bold",
        "italic",
        "link",
        "bulletedList",
        "numberedList",
        "-",
        "indent",
        "outdent",
        "blockQuote",
        "insertTable",
        "undo",
        "redo",
      ],
    })
      .then((newEditor) => {
        editor_alternative_c = newEditor;
      })
      .catch((error) => {
        console.error(error);
      });
  }
  if (alternative_d) {
    InlineEditor.create(alternative_d, {
      toolbar: [
        "heading",
        "bold",
        "italic",
        "link",
        "bulletedList",
        "numberedList",
        "-",
        "indent",
        "outdent",
        "blockQuote",
        "insertTable",
        "undo",
        "redo",
      ],
    })
      .then((newEditor) => {
        editor_alternative_d = newEditor;
      })
      .catch((error) => {
        console.error(error);
      });
  }
  if (alternative_e) {
    InlineEditor.create(alternative_e, {
      toolbar: [
        "heading",
        "bold",
        "italic",
        "link",
        "bulletedList",
        "numberedList",
        "-",
        "indent",
        "outdent",
        "blockQuote",
        "insertTable",
        "undo",
        "redo",
      ],
    })
      .then((newEditor) => {
        editor_alternative_e = newEditor;
      })
      .catch((error) => {
        console.error(error);
      });
  }
  $("#action-sub").on("click", function () {
    var validate = true;
    if (editor_alternative_a.getData().length > 0) {
      $("#question_alt_a").val(editor_alternative_a.getData());
      $("#alt_a_validate").hide();
    } else {
      $("#alt_a_validate").html(
        '<label class="error" for="question_tag_list">Este Campo es obligatorio</label>'
      );
      validate = false;
    }
    if (editor_alternative_b.getData().length > 0) {
      $("#question_alt_b").val(editor_alternative_b.getData());
      $("#alt_b_validate").hide();
    } else {
      $("#alt_b_validate").html(
        '<label class="error" for="question_tag_list">Este Campo es obligatorio</label>'
      );
      validate = false;
    }
    if (editor_alternative_c.getData().length > 0) {
      $("#question_alt_c").val(editor_alternative_c.getData());
      $("#alt_c_validate").hide();
    } else {
      $("#alt_c_validate").html(
        '<label class="error" for="question_tag_list">Este Campo es obligatorio</label>'
      );
      validate = false;
    }
    if (editor_alternative_d.getData().length > 0) {
      $("#question_alt_d").val(editor_alternative_d.getData());
      $("#alt_d_validate").hide();
    } else {
      $("#alt_d_validate").html(
        '<label class="error" for="question_tag_list">Este Campo es obligatorio</label>'
      );
      validate = false;
    }
    if (editor_alternative_e.getData().length > 0) {
      $("#question_alt_e").val(editor_alternative_e.getData());
      $("#alt_e_validate").hide();
    } else {
      $("#alt_e_validate").html(
        '<label class="error" for="question_tag_list">Este Campo es obligatorio</label>'
      );
      validate = false;
    }
    if (question_editor.getData().length > 0) {
      $("#question_definition").val(question_editor.getData());
      $("#questiondefinition_validate").hide();
    } else {
      $("#questiondefinition_validate").html(
        '<label class="error" for="question_tag_list">Este Campo es obligatorio</label>'
      );
      validate = false;
    }
    if (explanation_editor.getData().length > 0) {
      $("#question_explanation").val(explanation_editor.getData());
      $("#questionexplanation_validate").hide();
    } else {
      $("#questionexplanation_validate").html(
        '<label class="error" for="question_tag_list">Este Campo es obligatorio</label>'
      );
      validate = false;
    }

    console.log("data");
    var formulario = document.getElementById("question-form");
    //Aqui tu codigo para mostrar tus objetos
    if (validate) {
      $("#submit-question-form").click();
    }
  });
});
$(document).on("turbolinks:load", function () {});

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
const config = {
  $btnAddSubclase: $("#btn-add-subclase"),

  $containerSubClases: $("#div-container"),

  init() {
    this.initButtons();
  },

  initButtons() {
    this.$btnAddSubclase.on("click", () => {
      this.addSubclase(this.$containerSubClases);
    });
  },

  addSubclase($div) {
    let $subclase = $(`.cls-subclase {:style => "margin: 25px 25px 0px 25px;"}
      %label Clase
      %input.inp-descripcion{:type => "text}/
      %button.btn-add{:type => "button"} agregar
        .div-conteiner-subclases`);

    let $btn = $subclase.find(".btn-add");
    let $container = $subclase.find(".div-conteiner-subclases");

    $btn.on("click", () => {
      this.addSubclase($container);
    });

    $div.append($subclase);
  },
};

$(document).on("turbolinks:load", function () {
  ("use strict");
  window.addEventListener(
    "load",
    function () {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName("question-form");

      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function (form) {
        form.addEventListener(
          "submit",
          function (event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add("was-validated");
          },
          false
        );
      });
    },
    false
  );
});

$(document).ready(function () {
  setTimeout(function () {
    $(".alert").alert("close");
  }, 5000);
});

const images = require.context("../images", true);
const imagePath = (name) => images(name, true);
$(document).ready(function () {
  $("#pagination").DataTable({
    language: {
      sProcessing: "Procesando...",
      sLengthMenu: "Mostrar _MENU_ registros",
      sZeroRecords: "No se encontraron resultados",
      sEmptyTable: "Ningún dato disponible en esta tabla",
      sInfo:
        "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
      sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
      sInfoPostFix: "",
      sSearch: "Buscar:",
      sUrl: "",
      sInfoThousands: ",",
      sLoadingRecords: "Cargando...",
      oPaginate: {
        sFirst: "Primero",
        sLast: "Último",
        sNext: "Siguiente",
        sPrevious: "Anterior",
      },
      oAria: {
        sSortAscending:
          ": Activar para ordenar la columna de manera ascendente",
        sSortDescending:
          ": Activar para ordenar la columna de manera descendente",
      },
      buttons: {
        copy: "Copiar",
        colvis: "Visibilidad",
      },
    },
  });
});
