import $ from "jquery";
import "jquery.fancytree/dist/modules/jquery.fancytree.edit";
import "jquery.fancytree/dist/modules/jquery.fancytree";
import "jquery.fancytree/dist/modules/jquery.fancytree.filter";

$(document).on("turbolinks:load", function () {
  $("#edit_categories_tree").fancytree({
    extensions: ["edit"],
    selectMode: 3,
    source: $.ajax({
      url: "/categories/list.json",
      dataType: "json",
    }),
    cache: true,
    activate: function (event, data) {
      $("#statusLine").text(event.type + ": " + data.node);
    },
    select: function (event, data) {
      $("#statusLine").text(
        event.type + ": " + data.node.isSelected() + " " + data.node
      );
    },
    edit: {
      triggerStart: [
        "clickActive",
        "dblclick",
        "f2",
        "mac+enter",
        "shift+click",
      ],
      beforeEdit: function (event, data) {
        // Return false to prevent edit mode
      },
      edit: function (event, data) {
        // Editor was opened (available as data.input)
      },
      beforeClose: function (event, data) {
        // Return false to prevent cancel/save (data.input is available)
        //console.log("before");
        //if (data.originalEvent.type === "mousedown") {
        // We could prevent the mouse click from generating a blur event
        // (which would then again close the editor) and return `false` to keep
        // the editor open:
        //                  data.originalEvent.preventDefault();
        //                  return false;
        // Or go on with closing the editor, but discard any changes:
        //                  data.save = false;
        //}
      },
      save: function (event, data) {
        var category = {
          id: data.node.data.category_id,
          category_id: data.node.data.category_id,
          name: data.input.val(),
          parent_id: data.node.data.parent_id ? data.node.data.parent_id : null,
          level: data.node.data.level,
        };
        if (category.category_id) {
          $.ajax({
            url: `/categories/${category.id}.json`,
            method: "put",
            contentType: "application/json",
            data: JSON.stringify(category),
            success: function (response) {
              console.log(response);
            },
          });
        } else {
          $.ajax({
            url: "/categories.json",
            method: "post",
            contentType: "application/json",
            data: JSON.stringify(category),
            success: function (response) {
              data.node.data.category_id = response.category_id;
            },
          });
        }

        return true;
      },
      close: function (event, data) {
        // Editor was removed
        //if (data.save) {
        // Since we started an async request, mark the node as preliminary
        //  $(data.node.span).addClass("pending");
        //}
      },
    },
  });
  
  $("#blur_tree_button").on("click", function () {
    var tree = $.ui.fancytree.getTree();
    tree.setFocus(false);
  });

  $("#add_tree_node_button").on("click", function () {
    var tree = $.ui.fancytree.getTree();
    tree.getRootNode().editCreateNode("child", {
      title: "Nueva Categoría",
      parent_id: null,
      level: 0,
    });
    return;
  });

  $("#remove_node_button").on("click", function () {
    var tree = $.ui.fancytree.getTree(),
      node = tree.getActiveNode();
    console.log(node);
    if (!node) {
      alert("Debe Seleccionar Categoría");
      return;
    }

    $.ajax({
      url: `/categories/${node.data.category_id}.json`,
      method: "delete",
      contentType: "application/json",
      success: function (response) {
        node.remove();
        alert("La categoría fue eliminada.");
      },
      error: function (response) {
        alert(response.responseJSON.error);
      },
    });
  });

  $("#add_node_button").on("click", function () {
    var tree = $.ui.fancytree.getTree(),
      node = tree.getActiveNode();
    if (!node) {
      alert("Seleccionar Categoría");
      return;
    }

    node.editCreateNode("child", {
      title: "Nueva Categoría",
      level: node.data.level + 1,
      parent_id: node.data.category_id,
    });
  });

  $("#button2").click(function () {
    //aqui va codigo de eliminar
    var tree = $.ui.fancytree.getTree(),
      node = tree.getActiveNode();
    if (!node) {
      alert("Seleccione una categoria.");
      return;
    }
    console.log("Aqui se eliminan elementos del arbol de categorias");

    node.remove();
  });
  $("#button3").click(function () {
    var tree = $.ui.fancytree.getTree();
    var d = tree.toDict(true);
    console.log(d);
    console.log("Guardar un nuevo arbol de categorias!!!!!");
  });
});