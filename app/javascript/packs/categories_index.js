import $ from "jquery";
import "jquery.fancytree/dist/modules/jquery.fancytree";
import "jquery.fancytree/dist/modules/jquery.fancytree.filter";

$(document).on("turbolinks:load", function () {
  $("#index_categories_tree").fancytree({
    selectMode: 3,
    source: $.ajax({
      url: "/categories/list.json",
      dataType: "json",
    }),
    cache: true,
  });
});