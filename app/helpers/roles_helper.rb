# frozen_string_literal: true

module RolesHelper
  def users_attached?(role)
    User.where(role_id: role.id).present?
  end
end
