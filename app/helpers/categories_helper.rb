# frozen_string_literal: true

module CategoriesHelper
  def users_attached?(category)
    UserCategory.where(category_id: category.id).present?
  end
end
