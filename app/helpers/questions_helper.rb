# frozen_string_literal: true

module QuestionsHelper
  REVISIONS_LIMIT = 2
  COMMENTS_LIMIT = 100

  def can_be_observed?(question)
    question.revisions_limit < REVISIONS_LIMIT
  end

  def can_comment?(question)
    question.comments_limit < COMMENTS_LIMIT
  end

  def in_review?(question)
    question.status != 'incomplete'
  end

  def check_questions_limits(area, user_call)
    if area.name == 'Biomédicas'
      condition = user_call.biomedical_questions_counter.between?(0, user_call.biomedical_questions_limit - 1)
      user_call.biomedical_questions_counter += 1 if condition
      condition
    elsif area.name == 'Ingenierías'
      condition = user_call.engineering_questions_counter.between?(0, user_call.engineering_questions_limit - 1)
      user_call.engineering_questions_counter += 1 if condition
      condition
    elsif area.name == 'Sociales'
      condition = user_call.social_questions_counter.between?(0, user_call.social_questions_limit - 1)
      user_call.social_questions_counter += 1 if condition
      condition
    else
      false
    end
  end
end
