# frozen_string_literal: true

module UsersHelper
  def superadmin?(user)
    role = Role.where(name: 'super_admin').first
    user.role_id == role&.id
  end

  def questions_attached?(user)
    Question.where(pertinency_reviewer_id: user.id)
            .or(Question.where(format_reviewer_id: user.id))
            .or(Question.where(formulator_id: user.id)).present?
  end
end
