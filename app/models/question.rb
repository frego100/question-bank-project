# frozen_string_literal: true

class Question < ApplicationRecord
  has_paper_trail only: %i[definition explanation]
  acts_as_taggable_on :tags
  acts_as_paranoid

  has_many_attached :images

  belongs_to :category
  belongs_to :area
  belongs_to :formulator, class_name: 'User'
  belongs_to :pertinency_reviewer, class_name: 'User', optional: true
  belongs_to :format_reviewer, class_name: 'User', optional: true
  belongs_to :answer, class_name: 'Alternative', optional: true
  has_many :user_call_questions
  has_many :comments
  has_many :alternatives, dependent: :destroy

  enum status: { incomplete: 0, on_review: 1, observed: 2, rejected: 3, approved: 4 }
  enum difficulty: { low: 0, medium: 1, high: 2 }

  def image_urls
    return unless images.attachments

    image_urls = images.map do |image|
      Rails.application.routes.url_helpers.rails_blob_path(image, only_path: true)
    end

    image_urls
  end
end
