# frozen_string_literal: true

class UserCallQuestion < ApplicationRecord
  belongs_to :user_call, counter_cache: true
  belongs_to :question
end
