# frozen_string_literal: true

class Speciality < ApplicationRecord
  validates :name, presence: true, length: { in: 1..50 }
  has_many :category_specialities
  has_many :categories, through: :category_specialities

  has_many :users_specialities
  has_many :users, through: :users_specialities
end
