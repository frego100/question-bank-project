# frozen_string_literal: true

class Permission < ApplicationRecord
  validates :name, uniqueness: true, presence: true
  has_many :role_permissions
  has_many :roles, through: :role_permissions

  enum scope: { simple: 0, complex: 1 }
end
