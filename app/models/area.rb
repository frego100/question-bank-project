# frozen_string_literal: true

class Area < ApplicationRecord
  validates :name, uniqueness: true, presence: true
end
