# frozen_string_literal: true

class Call < ApplicationRecord
  validates :name, presence: true, length: { in: 1..50 }
  validates :starts_at, presence: true
  validates :ends_at, presence: true

  has_many :user_calls
  has_many :users, through: :user_calls

  enum status: { inactive: 0, active: 1 }

  before_create :set_status
  # after_create :create_jobs
  # after_update :update_jobs
  # after_destroy :delete_jobs

  def set_status
    self.status = 'inactive'
  end

  def delete_start_job
    Sidekiq::ScheduledSet.new.find_job(start_jid).delete
  end

  def delete_end_job
    Sidekiq::ScheduledSet.new.find_job(end_jid).delete
  end

  def create_jobs
    start_jid = StartCallWorker.perform_at(starts_at, id)
    end_jid = EndCallWorker.perform_at(ends_at, id)
    update(start_jid: start_jid, end_jid: end_jid)
  end

  def update_jobs
    if starts_at_changed?
      delete_start_job
      self.start_jid = StartCallWorker.perform_at(starts_at, id)
    end

    if ends_at_changed?
      delete_end_job
      self.end_jid = EndCallWorker.perform_at(ends_at, id)
    end
  end

  def delete_jobs
    delete_start_job
    delete_end_job
  end
end
