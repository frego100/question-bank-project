# frozen_string_literal: true

class Category < ApplicationRecord
  has_many :category_specialities
  has_many :specialities, through: :category_specialities
  has_many :questions
  belongs_to :parent, class_name: 'Category', optional: true
  has_many :subcategories, class_name: 'Category', foreign_key: 'parent_id'

  def as_fancytree_json
    {
      key: id,
      title: name,
      category_id: id,
      parent_id: parent_id
    }
  end
end
