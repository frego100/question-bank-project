# frozen_string_literal: true

class Candidate < ApplicationRecord
  has_one_attached :certificate
end
