# frozen_string_literal: true

class User < ApplicationRecord
  acts_as_paranoid

  validates :first_name, presence: true, format: { with: /[a-zA-Z]/, message: 'No debe ser un numero' }, length: { in: 1..20 }
  validates :last_name, presence: true, format: { with: /[a-zA-Z]/, message: 'No debe ser un numero' }, length: { in: 1..20 }
  validates :email, presence: true, format: { with: /\b[A-Z0-9._%a-z\-]+@+unsa\.edu\.pe\z/, message: 'No pertenece a la UNSA' }, length: { in: 1..30 }
  validates :password,
            format: { with: /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\W])/,
                      message: 'Debe de incluir un numero, letra mayuscula y un caracter especial' },
            length: { in: 1..100 },
            on: :create

  belongs_to :role

  has_many :users_specialities, dependent: :destroy
  has_many :specialities, through: :users_specialities
  has_many :user_calls

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable

  # validate :password_complexity

  def full_name
    "#{first_name} #{last_name}"
  end
end
