# frozen_string_literal: true

class EntranceExam < ApplicationRecord
  has_many :question_entrance_exams
  has_many :questions, through: :question_entrance_exams
end
