# frozen_string_literal: true

class Constants
  Alt = Struct.new(:id, :name)
  ALTERNATIVES = [
    Alt.new(0, 'Alternativa A'),
    Alt.new(1, 'Alternativa B'),
    Alt.new(2, 'Alternativa C'),
    Alt.new(3, 'Alternativa D'),
    Alt.new(4, 'Alternativa E')
  ].freeze
end
