# frozen_string_literal: true

class Alternative < ApplicationRecord
  has_paper_trail only: %i[description]
  acts_as_paranoid

  has_one_attached :image

  belongs_to :question

  def image_url
    return unless image.attachment

    Rails.application.routes.url_helpers.rails_blob_path(image, only_path: true)
  end
end
