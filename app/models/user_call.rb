# frozen_string_literal: true

class UserCall < ApplicationRecord
  belongs_to :user
  belongs_to :call
  has_many :user_call_categories
  has_many :categories, through: :user_call_categories
  has_many :user_call_questions
  has_many :questions, through: :user_call_questions

  validates_uniqueness_of :user_id, scope: :call_id
end
