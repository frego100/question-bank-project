# frozen_string_literal: true

class CategorySpeciality < ApplicationRecord
  belongs_to :category
  belongs_to :speciality
end
