# frozen_string_literal: true

class UsersSpeciality < ApplicationRecord
  belongs_to :user
  belongs_to :speciality
end
