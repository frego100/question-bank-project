# frozen_string_literal: true

class UserCallCategory < ApplicationRecord
  belongs_to :category
  belongs_to :user_call
end
