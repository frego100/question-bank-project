# frozen_string_literal: true

class Role < ApplicationRecord
  acts_as_paranoid

  validates :name, uniqueness: true, presence: true, format: { with: /[a-zA-Z]/ }, length: { in: 1..20 }
  has_many :role_permissions, dependent: :destroy
  has_many :permissions, through: :role_permissions
  has_many :users

  accepts_nested_attributes_for :permissions
end
