class QuestionTracksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_question_track, only: [:show, :edit, :update, :destroy]

  # GET /question_tracks
  # GET /question_tracks.json
  def index
    @question_tracks = QuestionTrack.all
  end

  # GET /question_tracks/1
  # GET /question_tracks/1.json
  def show
  end

  # GET /question_tracks/new
  def new
    @question_track = QuestionTrack.new
  end

  # GET /question_tracks/1/edit
  def edit
  end

  # POST /question_tracks
  # POST /question_tracks.json
  def create
    @question_track = QuestionTrack.new(question_track_params)

    respond_to do |format|
      if @question_track.save
        format.html { redirect_to @question_track, notice: 'Question track was successfully created.' }
        format.json { render :show, status: :created, location: @question_track }
      else
        format.html { render :new }
        format.json { render json: @question_track.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /question_tracks/1
  # PATCH/PUT /question_tracks/1.json
  def update
    respond_to do |format|
      if @question_track.update(question_track_params)
        format.html { redirect_to @question_track, notice: 'Question track was successfully updated.' }
        format.json { render :show, status: :ok, location: @question_track }
      else
        format.html { render :edit }
        format.json { render json: @question_track.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /question_tracks/1
  # DELETE /question_tracks/1.json
  def destroy
    @question_track.destroy
    respond_to do |format|
      format.html { redirect_to question_tracks_url, notice: 'Question track was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question_track
      @question_track = QuestionTrack.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def question_track_params
      params.fetch(:question_track, {})
    end
end
