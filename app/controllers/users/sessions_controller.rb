# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  before_action :authenticate_user!
  # before_action :configure_sign_in_params, only: [:create]
  # GET /resource/sign_in
  def new
    super
  end

  # POST /resource/sign_in
  def create
    email = params[:user][:email]
    if email.end_with? '@unsa.edu.pe'
      self.resource = warden.authenticate!(auth_options)
      set_flash_message!(:notice, :signed_in)
      sign_in(resource_name, resource)
      yield resource if block_given?
      respond_with resource, location: after_sign_in_path_for(resource)
    else
      respond_to do |format|
        format.html { redirect_to new_user_session_path, notice: 'Dominio inválido.' }
      end
    end
  end

  def after_sign_in_path_for(resource)
    if current_user.role_id == 1
      questions_path
    elsif current_user.role_id == 2
      questions_index_formulator_path
    elsif current_user.role_id == 3
      questions_index_operator_path
    elsif current_user.role_id == 4 || current_user.role_id == 5
      questions_index_reviewer_path
    end
  end
  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
