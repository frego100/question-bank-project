# frozen_string_literal: true

##
# Gestión de Convocatorias.

class CallsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_call, only: %i[show edit update destroy add_formulator add_pertinency_reviewer add_format_reviewer switch_status]
  before_action :connect_redis, only: %i[add_formulator add_pertinency_reviewer add_format_reviewer switch_status]

  require 'redis'

  # GET /calls
  # GET /calls.json

  ##
  # Retorna la lista de convocatorias.
  def index
    @calls = Call.all
    authorize @calls
  end

  # GET /calls/1
  # GET /calls/1.json

  ##
  # Redirige a la vista de agregación de usuarios para una convocatoria.
  def show
    authorize @call
    formulator_ids = UserPermission.where(permission_name: 'complex_formulator_permission').pluck(:user_id)
    pertinency_reviewer_ids = UserPermission.where(permission_name: 'complex_pertinency_reviewer_permission').pluck(:user_id)
    format_reviewer_ids = UserPermission.where(permission_name: 'complex_format_reviewer_permission').pluck(:user_id)

    @current_formulators_ids = @call.user_calls.where(user_id: formulator_ids).includes(:user)
    @current_pertinency_reviewers_ids = @call.user_calls.where(user_id: pertinency_reviewer_ids).includes(:user)
    @current_format_reviewers_ids = @call.user_calls.where(user_id: format_reviewer_ids).includes(:user)

    ids = formulator_ids - @current_formulators_ids.pluck(:user_id)
    @formulators = User.find(ids)

    remaining_pertinency_reviewers_ids = pertinency_reviewer_ids - @current_pertinency_reviewers_ids.pluck(:user_id)
    @remaining_pertinency_reviewers = User.find(remaining_pertinency_reviewers_ids)

    remaining_format_reviewers_ids = format_reviewer_ids - @current_format_reviewers_ids.pluck(:user_id)
    @remaining_format_reviewers = User.find(remaining_format_reviewers_ids)
    # Falta elegir bien las categorias
    @categories = Category.where(is_leaf: true)
  end

  # GET /calls/new

  ##
  # Redirige a la vista de nueva convocatoria.
  def new
    @call = Call.new
    authorize @call
  end

  # GET /calls/1/edit

  ##
  # Retorna la edición de convocatorias.
  def edit
    authorize @call
  end

  # POST /calls
  # POST /calls.json

  ##
  # Crea una nueva convocatoria.
  def create
    @call = Call.new(call_params)
    authorize @call

    respond_to do |format|
      if @call.starts_at > @call.ends_at
        format.html { redirect_to request.referer, alert: 'No puede existir fecha de inicio posterior a la de fin' }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      elsif @call.save
        format.html { redirect_to calls_path, notice: 'El proceso de formulación se creó correctamente.' }
        format.json { render :show, status: :created, location: @call }
      else
        format.html { render :new }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /calls/1
  # PATCH/PUT /calls/1.json

  ##
  # Actualiza una convocatoria.
  def update
    authorize @call
    respond_to do |format|
      if call_params['starts_at'] > call_params['ends_at']
        format.html { redirect_to request.referer, alert: 'No puede existir fecha de inicio posterior a la de fin' }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      elsif @call.update(call_params)
        format.html { redirect_to @call, notice: 'El proceso de formulación se actualizó correctamente.' }
        format.json { render :show, status: :ok, location: @call }
      else
        format.html { render :edit }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
    end
  end

  ##
  # Agrega un formulador a una convocatoria.
  def add_formulator
    authorize @call
    formulator_id = params[:call][:formulator_id]
    category_ids = params[:call][:category_ids] - ['']
    if formulator_id.blank? || category_ids.blank?
      respond_to do |format|
        format.html { redirect_to call_path(@call), alert: 'Something went wrong.' }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
      return
    end

    formulator = User.find(params[:call][:formulator_id])
    categories = Category.where(id: (params[:call][:category_ids]), is_leaf: true)

    user_call = UserCall.new
    user_call.user_id = formulator.id
    user_call.call_id = @call.id
    user_call.biomedical_questions_limit = params[:call][:biomedical_questions_limit]
    user_call.social_questions_limit = params[:call][:social_questions_limit]
    user_call.engineering_questions_limit = params[:call][:engineering_questions_limit]

    respond_to do |format|
      if user_call.save
        user_call.categories << categories
        @redis.rpush 'formulation_process_formulators', formulator.id if formulator.id
        FormulatorMailer.with(formulator_id: formulator.id, call_id: @call.id, user_call_id: user_call.id).summon_formulator_email.deliver_later
        format.html { redirect_to call_path(@call), notice: 'El proceso de formulación se actualizó correctamente.' }
        format.json { render :show, status: :ok, location: @call }
      else
        format.html { redirect_to call_path(@call), alert: 'Something went wrong.' }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
    end
  end

  ##
  # Agrega un revisor de pertinencia a una convocatoria.
  def add_pertinency_reviewer
    authorize @call
    pertinency_reviewer_id = params[:call][:pertinency_reviewer_id]
    if pertinency_reviewer_id.blank?
      respond_to do |format|
        format.html { redirect_to call_path(@call), alert: 'Something went wrong.' }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
      return
    end

    pertinency_reviewer = User.find(params[:call][:pertinency_reviewer_id])

    user_call = UserCall.new
    user_call.user_id = pertinency_reviewer.id
    user_call.call_id = @call.id

    respond_to do |format|
      if user_call.save
        @redis.rpush 'formulation_process_pertinency_reviewers', pertinency_reviewer.id if pertinency_reviewer.id
        format.html { redirect_to call_path(@call), notice: 'El revisor de pertinencia se añádió correctamente.' }
        format.json { render :show, status: :ok, location: @call }
      else
        format.html { redirect_to call_path(@call), alert: 'Something went wrong.' }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
    end
  end

  ##
  # Agrega un revisor de estilo a una convocatoria.
  def add_format_reviewer
    authorize @call
    format_reviewer_id = params[:call][:format_reviewer_id]
    if format_reviewer_id.blank?
      respond_to do |format|
        format.html { redirect_to call_path(@call), alert: 'Something went wrong.' }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
      return
    end

    format_reviewer = User.find(params[:call][:format_reviewer_id])

    user_call = UserCall.new
    user_call.user_id = format_reviewer.id
    user_call.call_id = @call.id

    respond_to do |format|
      if user_call.save
        @redis.rpush 'formulation_process_format_reviewers', format_reviewer.id if format_reviewer.id
        format.html { redirect_to call_path(@call), notice: 'El revisor de pertinencia se añádió correctamente.' }
        format.json { render :show, status: :ok, location: @call }
      else
        format.html { redirect_to call_path(@call), alert: 'Something went wrong.' }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /calls/1
  # DELETE /calls/1.json

  def destroy
    authorize @call
    @call.destroy
    respond_to do |format|
      format.html { redirect_to calls_url, notice: 'El proceso de formulación se eliminó con éxito.' }
      format.json { head :no_content }
    end
  end

  ##
  # Cambia es estado de una convocatoria, solo puede existir una convocatoria activa.
  def switch_status
    authorize @call
    formulator_ids = UserPermission.where(permission_name: 'complex_formulator_permission').pluck(:user_id)
    pertinency_reviewer_ids = UserPermission.where(permission_name: 'complex_pertinency_reviewer_permission').pluck(:user_id)
    format_reviewer_ids = UserPermission.where(permission_name: 'complex_format_reviewer_permission').pluck(:user_id)

    active_calls = Call.where(status: 'active')
    current_formulators = @call.user_calls.where(user_id: formulator_ids).pluck(:user_id)
    current_pertinency_reviewers = @call.user_calls.where(user_id: pertinency_reviewer_ids).pluck(:user_id)
    current_format_reviewers = @call.user_calls.where(user_id: format_reviewer_ids).pluck(:user_id)
    if @call.status == 'inactive' && active_calls.empty?
      @call.status = 'active'
      @redis.set 'is_formulation_process_active', true
      @redis.rpush 'formulation_process_formulators', current_formulators unless current_formulators.empty?
      @redis.rpush 'formulation_process_pertinency_reviewers', current_pertinency_reviewers unless current_pertinency_reviewers.empty?
      @redis.rpush 'formulation_process_format_reviewers', current_format_reviewers unless current_format_reviewers.empty?
    elsif @call.status == 'active'
      @call.status = 'inactive'
      active_calls = []
      @redis.del 'is_formulation_process_active'
      @redis.del 'formulation_process_formulators'
      @redis.del 'formulation_process_pertinency_reviewers'
      @redis.del 'formulation_process_format_reviewers'
    end

    respond_to do |format|
      if active_calls.empty?
        if @call.save
          format.html { redirect_to calls_url, notice: 'El proceso de formulación se actualizó correctamente.' }
          format.json { render json: { message: 'El proceso de formulación se actualizó correctamente.' }, status: :ok }
        else
          format.html { redirect_to calls_url, alert: 'No se pudo actualizar el proceso de formulación' }
          format.json { render json: @call.errors, status: :unprocessable_entity }
        end
      else
        format.html { redirect_to calls_url, alert: 'El proceso de formulación no se pudo actualizar debido a que hay un proceso de formulación activo.' }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
    end
  end

  ##
  # Edita las categorías asignadas a un formulador.
  def edit_formulator_categories
    # Need to improve this method
    call = Call.find(params[:call_id])
    user_call = UserCall.find(params[:user_call_id])
    category_ids = params[:user_call][:category_ids] - ['']
    category_ids = category_ids.map(&:to_i)
    current_ids = user_call.user_call_categories.pluck(:category_id)

    remove_categories = UserCallCategory.where(
      user_call_id: user_call.id,
      category_id: (current_ids - category_ids)
    )
    remove_categories.delete_all unless remove_categories.blank?

    add_categories = Category.where(id: (category_ids - current_ids), is_leaf: true)
    user_call.categories << add_categories unless add_categories.blank?

    respond_to do |format|
      FormulatorMailer.with(formulator_id: user_call.user_id, call_id: call.id, user_call_id: user_call.id).change_categories_email.deliver_later
      format.html { redirect_to call_path(call), notice: 'Las categorías de formuladores se actualizaron correctamente' }
      format.json { render json: { message: 'Las categorías de formuladores se actualizaron correctamente' }, status: :ok }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  ##
  # Establece la convocatoria para ser usada en otros métodos.
  def set_call
    @call = Call.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  ##
  # Retorna parámetros sanitizados para este controlador.
  def call_params
    params.require(:call).permit(:name, :starts_at, :ends_at)
  end
end
