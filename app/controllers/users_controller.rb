# frozen_string_literal: true

##
# Gestión de Usuarios.
class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: %i[show edit password_edit update update_password destroy]

  # GET /users
  # GET /users.json
  ##
  # Retorna la lista de usuarios y redirige a la lista de usuarios.
  def index
    @users = User.where.not(id: current_user.id)
    authorize @users
  end

  # GET /users/1
  # GET /users/1.json
  ##
  # Redirige a la vista de un usuario.
  def show
    authorize @user
  end

  # GET /users/new
  ##
  # Redirige al formulario de creación de nuevos usuarios.
  def new
    @user = User.new
    authorize @user
  end

  # GET /users/1/edit
  ##
  # Redirige a la vista de edición de usuarios.
  def edit
    authorize @user
  end

  ##
  # Redirige a la vista de cambio de contraseñas.
  def password_edit
    authorize @user
  end

  # POST /users
  # POST /users.json
  ##
  # Crea un nuevo usuario.
  def create
    email = params[:user][:email]

    if email.end_with? '@unsa.edu.pe'
      @user = User.new(new_user_params)
      @specialities = Speciality.where(id: params[:user][:speciality_ids])
      @user.specialities = @specialities

      authorize @user
      @user.password = Devise.friendly_token.first(12) + '!2'

      respond_to do |format|
        if @user.first_name.blank?
          format.html { redirect_to users_path, alert: 'No puede existir un usuario vacio' }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        elsif @user.save
          @user.send_reset_password_instructions
          format.html { redirect_to users_path, notice: 'Usuario creado correctamente.' }
          format.json { render :show, status: :created, location: @user }
        else
          format.html { redirect_to new_user_path, alert: 'Algo salió mal, vuelva a intentar.' }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    else
      puts 'Entro a excepcion'
      respond_to do |format|
        format.html { redirect_to new_user_path, alert: 'Correo institucional inválido.' }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  ##
  # Actualiza a los usuarios.
  def update
    authorize @user
    @specialities = Speciality.where(id: params[:user][:speciality_ids])
    respond_to do |format|
      if user_params['first_name'].blank?
        format.html { redirect_to @user, alert: 'No puede existir un usuario vacio' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      elsif @user.update(first_name: edit_user_params[:first_name], last_name: edit_user_params[:last_name], role_id: edit_user_params[:role_id], specialities: @specialities)
        format.html { redirect_to @user, notice: 'Los cambios se realizaron con éxito.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  ##
  # Actualiza una contraseña.
  def update_password
    authorize @user
    if @user.valid_password?(password_edit_user_params[:current_password])
      respond_to do |format|
        if @user.reset_password(password_edit_user_params[:password], password_edit_user_params[:password_confirmation])
          format.html { redirect_to @user, notice: 'Los cambios se realizaron con éxito.' }
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { redirect_to password_edit_user_path, alert: @user.errors }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to password_edit_user_path, alert: 'La contraseña actual no coincide' }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  ##
  # Elimina a un usuario.
  def destroy
    authorize @user

    if helpers.superadmin?(@user)
      respond_to do |format|
        format.html { redirect_to users_url, alert: "El usuario #{@user.first_name} es superadmin. No se puede eliminar" }
      end
      return
    end

    if helpers.questions_attached?(@user)
      respond_to do |format|
        format.html { redirect_to users_url, alert: "El usuario #{@user.first_name} está asignado a un proceso o pregunta. No se puede eliminar" }
      end
      return
    end

    respond_to do |format|
      format.html { redirect_to users_url, notice: "El usuario #{@user.first_name} fue eliminado correctamente" } if @user.destroy_fully!
    end
  end

  ##
  # Retorna la lista de revisores de pertinencia de la convocatoria activa.
  def pertinency_reviewers
    users_ids = current_call&.user_calls&.pluck(:user_id)
    users = User.joins(role: :permissions).where('permissions.name' => 'complex_pertinency_reviewer_permission').where(id: users_ids)
    render json: users.as_json(only: %i[id first_name last_name email])
  end

  ##
  # Retorna la lista de revisores de estilo de la convocatoria activa.
  def format_reviewers
    users_ids = current_call&.user_calls&.pluck(:user_id)
    users = User.joins(role: :permissions).where('permissions.name' => 'complex_format_reviewer_permission').where(id: users_ids)
    render json: users.as_json(only: %i[id first_name last_name email])
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(:first_name, :last_name, :email)
  end

  def edit_user_params
    params.require(:user).permit(:first_name, :last_name, :role_id)
  end

  def new_user_params
    params.require(:user).permit(:first_name, :last_name, :email, :role_id)
  end

  def password_edit_user_params
    params.require(:user).permit(:current_password, :password, :password_confirmation)
  end
end
