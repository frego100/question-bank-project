# frozen_string_literal: true

##
# Controlador para la redirección de usuarios según su permiso.
class WelcomeController < ApplicationController
  ##
  # Vista principal, según permiso.
  def index
    if current_user
      permissions = UserPermission.where(user_id: current_user.id)&.pluck(:permission_name)
      if permissions.include?('complex_super_admin_permission')
        redirect_to users_path
      elsif permissions.include?('complex_operator_permission')
        redirect_to questions_path
      elsif permissions.include?('complex_pertinency_reviewer_permission')
        redirect_to reviewer_status_path
      elsif permissions.include?('complex_format_reviewer_permission')
        redirect_to reviewer_status_path
      elsif permissions.include?('complex_formulator_permission')
        redirect_to formulator_status_path
      end
    else
      redirect_to new_user_session_path
    end
  end
end
