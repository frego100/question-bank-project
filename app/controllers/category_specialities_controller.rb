# frozen_string_literal: true

class CategorySpecialitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_category_speciality, only: %i[show edit update destroy]

  # GET /category_specialities
  # GET /category_specialities.json
  def index
    @category_specialities = CategorySpeciality.all
  end

  # GET /category_specialities/1
  # GET /category_specialities/1.json
  def show; end

  # GET /category_specialities/new
  def new
    @category_speciality = CategorySpeciality.new
  end

  # GET /category_specialities/1/edit
  def edit; end

  # POST /category_specialities
  # POST /category_specialities.json
  def create
    @category_speciality = CategorySpeciality.new(category_speciality_params)

    respond_to do |format|
      if @category_speciality.save
        format.html { redirect_to @category_speciality, notice: 'La especialidad de la categoría se creó con éxito.' }
        format.json { render :show, status: :created, location: @category_speciality }
      else
        format.html { render :new }
        format.json { render json: @category_speciality.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /category_specialities/1
  # PATCH/PUT /category_specialities/1.json
  def update
    respond_to do |format|
      if @category_speciality.update(category_speciality_params)
        format.html { redirect_to @category_speciality, notice: 'La especialidad de la categoría se actualizó correctamente.' }
        format.json { render :show, status: :ok, location: @category_speciality }
      else
        format.html { render :edit }
        format.json { render json: @category_speciality.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /category_specialities/1
  # DELETE /category_specialities/1.json
  def destroy
    @category_speciality.destroy
    respond_to do |format|
      format.html { redirect_to category_specialities_url, notice: 'La especialidad de la categoría fue eliminada con éxito.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_category_speciality
    @category_speciality = CategorySpeciality.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def category_speciality_params
    params.fetch(:category_speciality, {})
  end
end
