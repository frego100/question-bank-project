# frozen_string_literal: true

class UsersSpecialitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_users_speciality, only: %i[show edit update destroy]

  # GET /users_specialities
  # GET /users_specialities.json
  def index
    @users_specialities = UsersSpeciality.all
    authorize @users_specialities
  end

  # GET /users_specialities/1
  # GET /users_specialities/1.json
  def show; end

  # GET /users_specialities/new
  def new
    @users_speciality = UsersSpeciality.new
    authorize @users_speciality
  end

  # GET /users_specialities/1/edit
  def edit; end

  # POST /users_specialities
  # POST /users_specialities.json
  def create
    @users_speciality = UsersSpeciality.new(users_speciality_params)
    respond_to do |format|
      if @users_speciality.save
        format.html { redirect_to @users_speciality, notice: 'Users speciality was successfully created.' }
        format.json { render :show, status: :created, location: @users_speciality }
      else
        format.html { render :new }
        format.json { render json: @users_speciality.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users_specialities/1
  # PATCH/PUT /users_specialities/1.json
  def update
    respond_to do |format|
      if @users_speciality.update(users_speciality_params)
        format.html { redirect_to @users_speciality, notice: 'Users speciality was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_speciality }
      else
        format.html { render :edit }
        format.json { render json: @users_speciality.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users_specialities/1
  # DELETE /users_specialities/1.json
  def destroy
    @users_speciality.destroy
    respond_to do |format|
      format.html { redirect_to users_specialities_url, notice: 'Users speciality was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_users_speciality
    @users_speciality = UsersSpeciality.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def users_speciality_params
    params.fetch(:users_speciality, {})
  end
end
