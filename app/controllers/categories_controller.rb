# frozen_string_literal: true

##
# Gestión de categorías.
class CategoriesController < ApplicationController
  skip_before_action :verify_authenticity_token, only: %i[update create destroy]
  before_action :authenticate_user!
  before_action :set_category, only: %i[update destroy]

  ##
  # Retorna la lista de categorías.
  def list
    @categories = Category.where(level: 0)
                          .includes(
                            subcategories: { subcategories: { subcategories: :subcategories } }
                          ).order(:name)

    authorize @categories
  end

  ##
  # Redirige a la vista de edición de categorías.
  def edit_tree
    authorize current_user, policy_class: CategoryPolicy
  end

  # GET /categories
  # GET /categories.json
  ##
  # Redirige a la vista de categorías.
  def index
    authorize current_user, policy_class: CategoryPolicy
  end

  # GET /categories/1
  # GET /categories/1.json
  # def show; end

  # GET /categories/new
  # def new
  #   @category = Category.new
  # end

  # GET /categories/1/edit
  # def edit; end

  # POST /categories
  # POST /categories.json

  ##
  # Crea una categoría nueva.
  def create
    authorize current_user
    parent = Category.where(id: params[:parent_id]).first
    category = Category.new(new_category_params)
    category.is_leaf = true
    parent.is_leaf = false if parent

    respond_to do |format|
      if category.save
        parent&.save
        format.json { render json: category.as_fancytree_json, status: :ok }
      else
        format.json { render json: category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json

  ##
  # Actualiza una categoría.
  def update
    authorize @category
    respond_to do |format|
      if @category.update(category_params)
        format.json { render json: @category.as_fancytree_json, status: :ok }
      else
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json

  ##
  # Elimina una categoría.
  def destroy
    authorize @category
    question = Question.where(category_id: @category.id).first
    if !@category.is_leaf || question
      respond_to do |format|
        format.json do
          render json:
          { error: 'Solo puede eliminar categorías finales sobre las que no se hayan formulado preguntas' },
                 status: :unprocessable_entity
        end
      end
      return
    end

    if @category.destroy
      @category&.parent&.update(is_leaf: true)
      respond_to do |format|
        format.json { render json: @category.as_fancytree_json, status: :ok }
      end
    else
      respond_to do |format|
        format.json { render json: { error: 'No se pudo eliminar la pregunta.' }, status: :unprocessable_entity }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  ##
  # Establece una categoría para ser usada en el controlador.
  def set_category
    @category = Category.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  ##
  # Retorna parámetros sanitizados para categorías.
  def category_params
    params.permit(:name, :parent_id)
  end

  ##
  # Retorna parámetros sanitizados para categorías.
  def new_category_params
    params.permit(:name, :parent_id, :level)
  end
end
