# frozen_string_literal: true

##
# Gestión de comentarios.
class CommentsController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token
  before_action :set_comment, only: %i[update destroy]
  before_action :set_question, only: %i[index show create update destroy]

  # GET /comments.json

  ##
  # Retorna la lista de comentarios de una pregunta.
  def index
    authorize @question, policy_class: CommentPolicy
    @comments = @question.comments
  end

  # GET /comments/1.json

  ##
  # Retorna un comentario de una pregunta.
  def show
    authorize @question, policy_class: CommentPolicy
    @comment = @question.comments.find(params[:id])
  end

  # POST /comments
  # POST /comments.json
  ##
  # Crea un nuevo comentario a una pregunta.
  def create
    authorize @question, policy_class: CommentPolicy
    unless helpers.can_comment?(@question)
      render json: { errors: 'limite de comentarios alcanzado' }.to_json, status: :unprocessable_entity
      return
    end

    @comment = Comment.new(comment_params)
    @comment.question_id = @question.id
    total_comments = @question.comments_limit + 1

    if @comment.save
      @question.update_attribute(:comments_limit, total_comments)
      render json: @comment.as_json, status: :ok
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    authorize @question, policy_class: CommentPolicy
    if @comment.update(comment_params)
      render json: @comment.as_json, status: :ok
    else
      render json: { errors: '¡Algo salió mal!' }.to_json, status: :unprocessable_entity
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    authorize @question, policy_class: CommentPolicy
    if @comment.destroy
      render json: @comment.as_json, status: :ok
    else
      render json: { errors: '¡Algo salió mal!' }.to_json, status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_comment
    @comment = Comment.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def comment_params
    params.require(:comment).permit(:comment)
  end

  def set_question
    @question = Question.find(params[:question_id])
  end
end
