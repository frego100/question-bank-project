# frozen_string_literal: true

##
# Gestión de Roles.
class RolesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_role, only: %i[show edit update destroy]

  # GET /roles
  # GET /roles.json
  ##
  # Retorna la lista de roles y redirige a la lista de roles.
  def index
    @roles = Role.all
    authorize @roles
  end

  # GET /roles/1
  # GET /roles/1.json
  ##
  # Redirige a la vista de un rol.
  def show
    authorize @role
  end

  # GET /roles/new
  ##
  # Redirige al formulario para crear un nuevo rol.
  def new
    @role = Role.new
    authorize @role
  end

  # GET /roles/1/edit
  ##
  # Redirige a la vista para editar un rol.
  def edit
    authorize @role
  end

  # POST /roles
  # POST /roles.json
  ##
  # Crea un rol.
  def create
    @role = Role.new(name: params[:role][:name])
    @permissions = Permission.where(id: params[:role][:permission_ids])
    @role.permissions = @permissions
    respond_to do |format|
      if @role.save
        format.html { redirect_to @role, notice: "Rol #{@role.name} creado correctamente." }
        format.json { render :show, status: :created, location: @role }
      else
        format.html { render :new }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
    authorize @role
  end

  # PATCH/PUT /roles/1
  # PATCH/PUT /roles/1.json
  ##
  # Actualiza un rol.
  def update
    authorize @role
    permissions = Permission.where(id: params[:role][:permission_ids])
    respond_to do |format|
      if @role.update(name: edit_role_params[:name], permissions: permissions)
        format.html { redirect_to @role, notice: "Rol #{@role.name} modificado correctamente." }
        format.json { render :show, status: :ok, location: @role }
      else
        format.html { render :edit }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  ##
  # Elimina un rol si no fue usado.
  def destroy
    authorize @role

    if helpers.users_attached?(@role)
      respond_to do |format|
        format.html { redirect_to roles_url, alert: "El Rol #{@role.name} está asignado a un usuario. No se puede eliminar" }
      end
      return
    end

    if @role.destroy_fully!
      respond_to do |format|
        format.html { redirect_to roles_url, notice: "Rol #{@role.name} eliminado correctamente." }
      end
    else
      respond_to do |format|
        format.html { redirect_to roles_url, alert: "Rol #{@role.name} eliminado correctamente." }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_role
    @role = Role.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def role_params
    params.require(:role).permit(:name, :permission_ids)
  end

  def edit_role_params
    params.require(:role).permit(:name)
  end
end
