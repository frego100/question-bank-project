# frozen_string_literal: true

class SpecialitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_speciality, only: %i[show edit update destroy]

  # GET /specialities
  # GET /specialities.json
  def index
    @specialities = Speciality.all
  end

  # GET /specialities/1
  # GET /specialities/1.json
  def show; end

  # GET /specialities/new
  def new
    @speciality = Speciality.new
  end

  # GET /specialities/1/edit
  def edit; end

  # POST /specialities
  # POST /specialities.json
  def create
    @speciality = Speciality.new(speciality_params)
    authorize @speciality

    respond_to do |format|
      if @speciality.name.blank?
        format.html { redirect_to @speciality, alert: 'No puede existir una especialidad vacia' }
        format.json { render json: @speciality.errors, status: :unprocessable_entity }
      elsif @speciality.save
        format.html { redirect_to @speciality, notice: 'La especialidad se creó con éxito.' }
        format.json { render :show, status: :created, location: @speciality }
      else
        format.html { render :new }
        format.json { render json: @speciality.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /specialities/1
  # PATCH/PUT /specialities/1.json
  def update
    authorize @speciality

    respond_to do |format|
      if speciality_params['name'].blank?
        format.html { redirect_to @speciality, alert: 'No puede existir una especialidad vacia' }
        format.json { render json: @speciality.errors, status: :unprocessable_entity }
      elsif @speciality.update(speciality_params)
        format.html { redirect_to @speciality, notice: 'La especialidad se actualizó con éxito.' }
        format.json { render :show, status: :ok, location: @speciality }
      else
        format.html { render :edit }
        format.json { render json: @speciality.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /specialities/1
  # DELETE /specialities/1.json
  def destroy
    @speciality.destroy
    respond_to do |format|
      format.html { redirect_to specialities_url, notice: 'La especialidad fue eliminada con éxito.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_speciality
    @speciality = Speciality.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def speciality_params
    params.require(:speciality).permit(:name)
  end
end
