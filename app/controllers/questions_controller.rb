# frozen_string_literal: true
##
# Gestión de preguntas.
class QuestionsController < ApplicationController
  before_action :authenticate_user!
  before_action :verify_formulation_process, except: %i[formulator_status reviewer_status]
  # before_action :check_questions_limit, only: %i[new create]
  before_action :set_question, only: %i[show edit update destroy send_question_to_review
                                        reject_question approve_question assign_question observe_question]
  skip_before_action :verify_authenticity_token, only: [:assign_question]
  before_action :set_paper_trail_whodunnit

  # GET /questions
  # GET /questions.json
  ##
  # Retorna la lista de preguntas aprovadas y redirige a la vista correspondiente.
  def index
    @questions = Question.where(status: APPROVED)
    authorize @questions
  end

  ##
  # Retorna las preguntas en revisión y redirige a la vista del operador.
  def index_operator
    table = Question.arel_table
    sentence = table[:status]
               .eq(ON_REVIEW)
               .and(
                 table[:pertinency_reviewer_id].eq(nil).and(table[:pertinency_approved_at].eq(nil))
                 .or(
                   table[:pertinency_reviewer_id].not_eq(nil).and(table[:pertinency_approved_at].not_eq(nil)).and(
                     table[:format_reviewer_id].eq(nil).and(table[:format_approved_at].eq(nil))
                   )
                 )
                 .or(
                   table[:pertinency_reviewer_id].not_eq(nil).and(table[:pertinency_approved_at].not_eq(nil)).and(
                     table[:format_reviewer_id].not_eq(nil).and(table[:format_approved_at].not_eq(nil))
                   )
                 )
               )

    @questions = Question.where(sentence).order(:created_at)

    authorize @questions
  end

  ##
  # Retorna las preguntas asignadas a un revisor y redirige a la vista del revisor.
  def index_reviewer
    table = Question.arel_table
    sentence = table[:status].eq(ON_REVIEW).and(
      table[:pertinency_reviewer_id].eq(current_user.id).and(table[:pertinency_approved_at].eq(nil))
      .or(
        table[:pertinency_reviewer_id].not_eq(nil).and(table[:pertinency_approved_at].not_eq(nil)).and(
          table[:format_reviewer_id].eq(current_user.id).and(table[:format_approved_at].eq(nil))
        )
      )
    )

    @questions = Question.where(sentence)
    authorize @questions
  end

  ##
  # Retorna las preguntas incompletas de un formulador y redirige a la vista del formulador.
  def index_formulator
    @questions = if params[:tag]
                   Question.where(status: [INCOMPLETE, OBSERVED]).where(formulator_id: current_user.id).tagged_with(params[:tag]).includes(%i[category area])
                 else
                   Question.where(status: [INCOMPLETE, OBSERVED]).where(formulator_id: current_user.id).includes(%i[category area])
                 end
    authorize @questions
  end

  # GET /questions/1/edit
  def edit_formulator; end

  # GET /questions/1/edit
  def edit_reviewer; end

  # GET /questions/1
  # GET /questions/1.json
  ##
  # Redirige a la vista de una pregunta.
  def show
    authorize @question
  end

  # GET /questions/new
  ##
  # Redirige al formulario de creación de una nueva pregunta.
  def new
    @question = Question.new
    authorize @question
    @categories = current_call&.user_calls&.where(user_id: current_user.id)&.first&.categories
  end

  # GET /questions/1/edit
  ##
  # Redirige a la vista de edición de una pregunta.
  def edit
    authorize @question
    @categories = current_call&.user_calls&.where(user_id: current_user.id)&.first&.categories
  end

  # POST /questions
  # POST /questions.json
  ##
  # Crea una nueva pregunta.
  def create
    @question = Question.new(new_question_params)
    authorize @question
    area = Area.find(new_question_params[:area_id])
    user_call = current_call.user_calls.where(user_id: current_user.id).first
    unless helpers.check_questions_limits(area, user_call)
      respond_to do |format|
        format.html do
          redirect_to questions_index_formulator_path,
                      alert: "Estás en el límite de preguntas de #{area.name}. Ya no puedes formular más preguntas de esa área."
        end
      end
      return
    end

    @question.formulator = current_user
    @question.status = INCOMPLETE
    # @question.tag_list = new_question_params[:tag_list]
    respond_to do |format|
      if user_call.save && @question.save
        UserCallQuestion.create(question_id: @question.id, user_call_id: user_call.id)
        alternatives = [
          Alternative.new(description: alternatives_params[:alt_a], image: alternatives_params[:alt_a_image], order: 0),
          Alternative.new(description: alternatives_params[:alt_b], image: alternatives_params[:alt_b_image], order: 1),
          Alternative.new(description: alternatives_params[:alt_c], image: alternatives_params[:alt_c_image], order: 2),
          Alternative.new(description: alternatives_params[:alt_d], image: alternatives_params[:alt_d_image], order: 3),
          Alternative.new(description: alternatives_params[:alt_e], image: alternatives_params[:alt_e_image], order: 4)
        ]

        @question.alternatives = alternatives
        @question.answer_id = alternatives[alternatives_params[:order].to_i].id
        @question.save

        format.html { redirect_to edit_question_path(@question), notice: 'La pregunta se creó correctamente.' }
        format.json { render :show, status: :created, location: @question }
      else
        format.html { render :new }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  ##
  # Actualiza una pregunta.
  def update
    authorize @question
    alternatives = @question.alternatives.order(:order)
    alternatives[0].update(description: alternatives_params[:alt_a], image: alternatives_params[:alt_a_image])
    alternatives[1].update(description: alternatives_params[:alt_b], image: alternatives_params[:alt_b_image])
    alternatives[2].update(description: alternatives_params[:alt_c], image: alternatives_params[:alt_c_image])
    alternatives[3].update(description: alternatives_params[:alt_d], image: alternatives_params[:alt_d_image])
    alternatives[4].update(description: alternatives_params[:alt_e], image: alternatives_params[:alt_e_image])

    @question.assign_attributes(edit_question_params)
    @question.answer_id = alternatives[alternatives_params[:order].to_i].id
    # @question.tag_list = edit_question_params[:tag_list]
    respond_to do |format|
      if @question.update(edit_question_params)
        format.html { redirect_to edit_question_path(@question), notice: 'La pregunta se actualizó correctamente.' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  ##
  # Elimina una pregunta.
  def destroy
    authorize @question

    unless @question.incomplete?
      respond_to do |format|
        format.html { redirect_to questions_index_formulator_path, alert: 'La pregunta está en revisión. No se puede eliminar' }
      end
      return
    end

    if @question.destroy
      respond_to do |format|
        format.html { redirect_to questions_index_formulator_path, notice: 'La pregunta fue eliminada exitósamente' }
      end
    else
      respond_to do |format|
        format.html { redirect_to questions_index_formulator_path, alert: 'No se pudo eliminar la pregunta' }
      end
    end
  end

  ##
  # Asigna la pregunta a un revisor de pertinencia o estilo, según sea el caso.
  def assign_question
    reviewer = User.find(params[:reviewer_id])
    question = Question.find(params[:question_id])

    if !question.pertinency_approved_at &&
       !question.format_approved_at &&
       reviewer.role.permissions.pluck(:name).include?('complex_pertinency_reviewer_permission')

      question.pertinency_reviewer_id = reviewer.id
      question.status = ON_REVIEW
      if question.save
        render json: { message: 'Éxito' }.to_json, status: :ok
      else
        render json: { message: '¡Algo salió mal!' }.to_json, status: :unprocessable_entity
      end

    elsif question.pertinency_approved_at && !question.format_approved_at &&
          reviewer.role.permissions.pluck(:name).include?('complex_format_reviewer_permission')

      question.format_reviewer_id = reviewer.id
      question.status = ON_REVIEW
      if question.save
        render json: { message: 'Éxito' }.to_json, status: :ok
      else
        render json: { message: '¡Algo salió mal!' }.to_json, status: :unprocessable_entity
      end
    end

    # authorize reviewer
    # if @question.pertinency_approved_at.blank? &&  @question.format_approved_at.blank?
    # reviewer.role.permissions.
  end

  ##
  # Aprueba una pregunta para que pase a la siguiente fase.
  def approve_question
    # validar formulador, estado, ...
    authorize @question
    if @question.pertinency_approved_at.blank?
      @question.pertinency_approved_at = Time.now
      if @question.save
        render json: { location: questions_index_reviewer_url, message: 'Éxito' }.to_json, status: :ok
      else
        render json: { message: '¡Algo salió mal!' }.to_json, status: :unprocessable_entity
      end
    elsif @question.format_approved_at.blank?
      @question.format_approved_at = Time.now
      if @question.save
        render json: { location: questions_index_reviewer_url, message: 'Éxito' }.to_json, status: :ok
      else
        render json: { message: '¡Algo salió mal!' }.to_json, status: :unprocessable_entity
      end
    elsif @question.on_review?
      @question.status = APPROVED
      if @question.save
        render json: { location: questions_index_operator_url, message: 'Éxito' }.to_json, status: :ok
      else
        render json: { message: '¡Algo salió mal!' }.to_json, status: :unprocessable_entity
      end
    else
      render json: { message: '¡Algo salió mal!' }.to_json, status: :unprocessable_entity
    end
  end

  ##
  # Rechaza una pregunta.
  def reject_question
    authorize @question
    @question.status = REJECTED
    respond_to do |format|
      if @question.save
        format.html { redirect_to questions_index_operator_url, notice: 'La pregunta fue rechazada correctamente.' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  ##
  # Envía una pregunta creada por un formudor al operador, por lo tanto se agrega a su lista.
  def send_question_to_review
    # AQUI SE VALIDA PRONTO
    authorize @question
    @question.status = ON_REVIEW
    respond_to do |format|
      if @question.save
        format.html { redirect_to questions_index_formulator_path, notice: 'La pregunta fue enviada correctamente.' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  ##
  # Un revisor de pertinencia puede observar una pregunta.
  def observe_question
    authorize @question
    unless helpers.can_be_observed?(@question)
      render json: { location: questions_index_reviewer_url, error: 'La pregunta excedió el número de observaciones permitidas' }.to_json, status: :ok
      return
    end

    @question.status = OBSERVED
    @question.revisions_limit = @question.revisions_limit + 1

    if @question.save
      FormulatorMailer.with(question_id: @question.id).observed_question_email.deliver_later
      render json: { location: questions_index_reviewer_url, message: 'La pregunta fue observada exitosamente.' }.to_json, status: :ok
    else
      render json: { message: '¡Algo salió mal!' }.to_json, status: :unprocessable_entity
    end
  end

  ##
  # Redirige a la vista del estado de un formulador.
  def formulator_status
    authorize current_user, policy_class: QuestionPolicy
    @questions_counter = Question.where(formulator_id: current_user.id).count
  end

  ##
  # Redirige a la vista el estado de un revisor.
  def reviewer_status
    authorize current_user, policy_class: QuestionPolicy
    @pertinency_reviewed_counter = Question.where(pertinency_reviewer_id: current_user.id).count
    @format_reviewed_counter = Question.where(format_reviewer_id: current_user.id).count
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_question
    @question = Question.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def question_params
    params.require(:question).permit(:reviewer_id, :status, :definition, :answer_id, :deleted_at, :format_approved_at, :pertinency_approved_at)
  end

  def new_question_params
    params.require(:question).permit(:area_id, :category_id, :difficulty, :definition, :explanation, images: [])
  end

  def question_edit_formulator_params
    params.require(:question).permit(:category_id, :difficulty, :definition, :explanation, images: [])
  end

  def edit_question_params
    params.require(:question).permit(:area_id, :category_id, :difficulty, :definition, :explanation, images: [])
  end

  def alternatives_params
    params.require(:question).permit(:alt_a, :alt_b, :alt_c, :alt_d, :alt_e,
                                     :alt_a_image, :alt_b_image,
                                     :alt_c_image, :alt_d_image,
                                     :alt_e_image, :order)
  end

  def check_questions_limit
    active_calls = Call.where(status: 'active')

    raise StandardError, 'Zero or More than 1 active formulation process found' if active_calls.size != 1

    active_call = active_calls.first
    if active_call
      user_calls = UserCall.where(user_id: current_user.id, call_id: active_call.id)
      user_call = user_calls.first
      questions_limit = user_call.questions_limit
    end

    questions = Question.where(formulator_id: current_user.id).size
    if questions < questions_limit
      true
    else
      respond_to do |format|
        format.html { redirect_to questions_index_formulator_path, alert: 'Usted ha alcanzado el número máximo de preguntas que puede formular.' }
      end
    end
  end
end
