class AcceptanceProcessesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_acceptance_process, only: [:show, :edit, :update, :destroy]

  # GET /acceptance_processes
  # GET /acceptance_processes.json
  def index
    @acceptance_processes = AcceptanceProcess.all
  end

  # GET /acceptance_processes/1
  # GET /acceptance_processes/1.json
  def show
  end

  # GET /acceptance_processes/new
  def new
    @acceptance_process = AcceptanceProcess.new
  end

  # GET /acceptance_processes/1/edit
  def edit
  end

  # POST /acceptance_processes
  # POST /acceptance_processes.json
  def create
    @acceptance_process = AcceptanceProcess.new(acceptance_process_params)

    respond_to do |format|
      if @acceptance_process.save
        format.html { redirect_to @acceptance_process, notice: 'Acceptance process was successfully created.' }
        format.json { render :show, status: :created, location: @acceptance_process }
      else
        format.html { render :new }
        format.json { render json: @acceptance_process.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /acceptance_processes/1
  # PATCH/PUT /acceptance_processes/1.json
  def update
    respond_to do |format|
      if @acceptance_process.update(acceptance_process_params)
        format.html { redirect_to @acceptance_process, notice: 'Acceptance process was successfully updated.' }
        format.json { render :show, status: :ok, location: @acceptance_process }
      else
        format.html { render :edit }
        format.json { render json: @acceptance_process.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /acceptance_processes/1
  # DELETE /acceptance_processes/1.json
  def destroy
    @acceptance_process.destroy
    respond_to do |format|
      format.html { redirect_to acceptance_processes_url, notice: 'Acceptance process was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_acceptance_process
      @acceptance_process = AcceptanceProcess.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def acceptance_process_params
      params.require(:acceptance_process).permit(:description)
    end
end
