# frozen_string_literal: true

##
# Gestión de Exámenes.
class EntranceExamsController < ApplicationController
  before_action :authenticate_user!
  # before_action :verify_formulation_process, except: %i[formulator_status reviewer_status]
  # before_action :check_questions_limit, only: %i[new create]
  before_action :set_entrance_exam, only: %i[show edit update destroy]
  # skip_before_action :verify_authenticity_token, only: [:assign_question]
  # before_action :set_paper_trail_whodunnit

  # GET /questions
  # GET /questions.json
  ##
  # Retorna la lista de exámenes.
  def index
    @entrance_exams = EntranceExam.all
    authorize @entrance_exams
  end

  # GET /questions/1
  # GET /questions/1.json
  ##
  # Redirige a la vista de agregación de preguntas para un exámen.
  def show
    authorize @entrance_exam
    # @q = Question.ransack(params[:q])
    # @questions = @q.result(distinct: true)
    ids = @entrance_exam.question_entrance_exams.pluck(:question_id)
    @questions = Question.where.not(id: ids).where(status: APPROVED).includes(:category).order(:created_at)
    @added_questions = Question.where(id: ids).includes(:category).order(:created_at)
  end

  # GET /questions/new
  ##
  # Redirige a la vista de creación de un nuevo exámen.
  def new
    @entrance_exam = EntranceExam.new
    authorize @entrance_exam
  end

  # GET /questions/1/edit
  def edit
    authorize @entrance_exam
  end

  # POST /questions
  # POST /questions.json
  ##
  # Crea un nuevo exámen.
  def create
    @entrance_exam = EntranceExam.new(entrance_exam_params)
    authorize @entrance_exam
    if @entrance_exam.save
      respond_to do |format|
        format.html { redirect_to entrance_exams_path, notice: 'Exámen de entrada creado exitosamente.' }
      end
    else
      respond_to do |format|
        format.html { redirect_to entrance_exams_path, alert: 'El exámen no pudo ser creado.' }
      end
    end
  end

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  def update
    authorize @entrance_exam
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    authorize @entrance_exam

    unless @entrance_exam.incomplete?
      respond_to do |format|
        format.html { redirect_to questions_index_formulator_path, alert: 'La pregunta está en revisión. No se puede eliminar' }
      end
      return
    end

    if @entrance_exam.destroy
      respond_to do |format|
        format.html { redirect_to questions_index_formulator_path, notice: 'La pregunta fue eliminada exitósamente' }
      end
    else
      respond_to do |format|
        format.html { redirect_to questions_index_formulator_path, alert: 'No se pudo eliminar la pregunta' }
      end
    end
  end

  ##
  # Agrega una pregunta aprovada a un exámen.
  def add_question
    entrance_exam = EntranceExam.find(params[:entrance_exam_id])
    authorize entrance_exam
    # if @entrance_exam.pertinency_approved_at.blank? &&  @entrance_exam.format_approved_at.blank?
    # reviewer.role.permissions.
    question = Question.find(params[:question_id])
    question_entrance_exam = QuestionEntranceExam.new(question_id: question.id, entrance_exam_id: entrance_exam.id)
    if question_entrance_exam.save
      respond_to do |format|
        format.html { redirect_to entrance_exam_path(entrance_exam), notice: 'Pregunta agregada exitosamente.' }
        format.json { render json: { message: 'Agregado Exitosamente' }.to_json, status: :ok }
      end
    else
      respond_to do |format|
        format.html { redirect_to entrance_exams_path(entrance_exam), notice: 'No se pudo agregar pregunta.' }
        format.json { render json: { error: 'Algo salió mal.' }.to_json, status: :unprocessable_entity }
      end
    end
  end

  ##
  # Quita una pregunta aprovada de un exámen.
  def remove_question
    entrance_exam = EntranceExam.find(params[:entrance_exam_id])
    authorize entrance_exam
    question = Question.find(params[:question_id])
    entrance_exam_question = QuestionEntranceExam.where(question_id: question.id, entrance_exam_id: entrance_exam.id).first
    if entrance_exam_question.destroy
      respond_to do |format|
        format.html { redirect_to entrance_exams_path, notice: 'Pregunta quitada exitosamente.' }
        format.json { render json: { message: 'Quitado Exitosamente' }.to_json, status: :ok }
      end
    else
      respond_to do |format|
        format.html { redirect_to entrance_exams_path, notice: 'No se pudo quitar pregunta.' }
        format.json { render json: { error: 'Algo salió mal.' }.to_json, status: :unprocessable_entity }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_entrance_exam
    @entrance_exam = EntranceExam.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def entrance_exam_params
    params.require(:entrance_exam).permit(:name, :description, :tooked_at)
  end
end
