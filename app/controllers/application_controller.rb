# frozen_string_literal: true

##
# Controlador padre.

class ApplicationController < ActionController::Base
  include Pundit
  require 'redis'

  ##
  # Error personalizado para autorización de entrada al formulador.

  class NotAuthorizedFormulatorError < StandardError
    def message
      "Currently, you're not authorized to access the system"
    end
  end

  ##
  # Error personalizado para verificación de convocatoria activa.

  class NotActivatedCallError < StandardError
    def message
      'No hay una convocatoria activa. Contáctese con su administrador si el problema persiste.'
    end
  end

  ##
  # Símbolo para usarse en todos los controladores, es el estado de una pregunta +incomplete+.

  INCOMPLETE = :incomplete

  ##
  # Símbolo para usarse en todos los controladores, es el estado de una pregunta +on_review+.

  ON_REVIEW = :on_review

  ##
  # Símbolo para usarse en todos los controladores, es el estado de una pregunta +observed+.

  OBSERVED = :observed

  ##
  # Símbolo para usarse en todos los controladores, es el estado de una pregunta +rejected+.
  REJECTED = :rejected

  ##
  # Símbolo para usarse en todos los controladores, es el estado de una pregunta +approved+.
  APPROVED = :approved

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  rescue_from ApplicationController::NotAuthorizedFormulatorError, with: :user_not_listed
  rescue_from ApplicationController::NotActivatedCallError, with: :user_not_authorized

  ##
  # Retorna la convocatoria activa actual.
  #

  def current_call
    call = Call.where(status: :active).first
    raise ApplicationController::NotActivatedCallError unless call.present?

    call
  end

  private

  ##
  # Método para verificar si el usuario que inicia sesión está autorizado a realizar una acción
  # Lanza la exepción +ApplicationController::NotActivatedCallError+
  #

  def user_not_authorized
    respond_to do |format|
      format.html { redirect_to root_path, alert: 'No estás autorizado para realizar esta acción.' }
      format.xml  { head :forbidden }
      format.any  { head :forbidden }
    end
  end


  ##
  # Método para verificar si el usuario que inició sesión, está en la lista de la convocatoria activa.
  # Lanza la exepción +ApplicationController::NotAuthorizedFormulatorError+
  #

  def user_not_listed
    respond_to do |format|
      format.html { redirect_to root_path, alert: 'No estás en la lista de convocados o la convocatoria no está activa.' }
      format.xml  { head :forbidden }
      format.any  { head :forbidden }
    end
  end

  ##
  # Método para realizar verificar la conexión con Redis
  #

  def connect_redis
    @redis = Redis.new(db: 0)
    @redis.ping
  rescue RuntimeError
    respond_to do |format|
      format.html { redirect_to root_path, alert: 'Debes conectar Redis.' }
    end
  end

  ##
  # Método que verifica si el usuario está en dentro de la convocatoria activa por medio de Redis.
  # Lanza la exepción +ApplicationController::NotAuthorizedFormulatorError+
  #

  def verify_formulation_process
    redis = Redis.new(db: 0)
    is_formulation_process_active = redis.exists 'is_formulation_process_active'
    # roles that have to be checked before access the system
    permissions = %w[complex_formulator_permission complex_pertinency_reviewer_permission complex_format_reviewer_permission]
    return true unless UserPermission.where(user_id: current_user.id, permission_name: permissions).present?

    if UserPolicy.new(current_user, current_user).is_formulator?
      is_formulator_in_process = redis.lrange('formulation_process_formulators', 0, -1).include? current_user.id.to_s
      return true if is_formulator_in_process && is_formulation_process_active

    elsif UserPolicy.new(current_user, current_user).is_pertinency_reviewer?
      is_pertinency_reviewer_in_process = redis.lrange('formulation_process_pertinency_reviewers', 0, -1).include? current_user.id.to_s
      return true if is_pertinency_reviewer_in_process && is_formulation_process_active

    elsif UserPolicy.new(current_user, current_user).is_format_reviewer?
      is_format_reviewer_in_process = redis.lrange('formulation_process_format_reviewers', 0, -1).include? current_user.id.to_s
      return true if is_format_reviewer_in_process && is_formulation_process_active

    end
    raise ApplicationController::NotAuthorizedFormulatorError
  end
end
