# frozen_string_literal: true

class StartCallWorker
  include Sidekiq::Worker

  def perform(call_id)
    puts 'Starting new Call'
    call = Call.find(call_id)
    if call.update(status: 'active', started_at: Time.now)
      puts 'CALL STARTED'
    else
      puts 'SOMETHING WENT WRONG'
    end
    # Do something
  end
end
