# frozen_string_literal: true

class EndCallWorker
  include Sidekiq::Worker

  def perform(call_id)
    puts 'Ending Call'
    call = Call.find(call_id)
    if call.update(status: 'inactive', ended_at: Time.now)
      puts 'CALL ENDED'
    else
      puts 'SOMETHING WENT WRONG'
    end
    # Do something
  end
end
