json.extract! users_speciality, :id, :created_at, :updated_at
json.url users_speciality_url(users_speciality, format: :json)
