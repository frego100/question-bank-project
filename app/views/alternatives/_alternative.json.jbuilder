json.extract! alternative, :id, :description, :images, :formula, :created_at, :updated_at
json.url alternative_url(alternative, format: :json)
