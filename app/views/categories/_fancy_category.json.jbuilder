# frozen_string_literal: true

json.title category.name
json.key category.id
json.category_id category.id
json.parent_id category.parent_id
json.level category.level
unless category.is_leaf
  json.children do
    json.array! category.subcategories, partial: 'categories/fancy_category', as: :category
  end
end
