# frozen_string_literal: true

json.array! @categories, partial: 'categories/fancy_category', as: :category
