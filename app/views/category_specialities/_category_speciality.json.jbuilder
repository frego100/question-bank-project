json.extract! category_speciality, :id, :created_at, :updated_at
json.url category_speciality_url(category_speciality, format: :json)
