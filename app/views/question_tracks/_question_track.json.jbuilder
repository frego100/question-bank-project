json.extract! question_track, :id, :created_at, :updated_at
json.url question_track_url(question_track, format: :json)
