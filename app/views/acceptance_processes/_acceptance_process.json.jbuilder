json.extract! acceptance_process, :id, :description, :created_at, :updated_at
json.url acceptance_process_url(acceptance_process, format: :json)
