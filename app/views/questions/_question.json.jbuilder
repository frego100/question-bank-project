# frozen_string_literal: true

json.extract! question, :id, :pertinency_reviewer_id, :format_reviewer_id, :explanation, :status, :difficulty, :definition, :answer_id, :deleted_at, :format_approved_at, :pertinency_approved_at, :created_at, :updated_at, :image_urls
json.alternatives question.alternatives do |alt|
  json.id alt.id
  json.question_id alt.question_id
  json.description alt.description
  json.formula alt.formula
  json.order alt.order
  json.image_url alt.image_url
end
json.category question.category
json.url question_url(question, format: :json)
