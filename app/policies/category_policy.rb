# frozen_string_literal: true

class CategoryPolicy < ApplicationPolicy
  def list?
    UserPermission.where(user_id: user.id, permission_name: 'category_list').present?
  end

  def index?
    UserPermission.where(user_id: user.id, permission_name: 'category_index').present?
  end

  def edit_tree?
    UserPermission.where(user_id: user.id, permission_name: 'category_edit_tree').present?
  end

  def create?
    UserPermission.where(user_id: user.id, permission_name: 'category_create').present?
  end

  def update?
    UserPermission.where(user_id: user.id, permission_name: 'category_update').present?
  end

  def destroy?
    UserPermission.where(user_id: user.id, permission_name: 'category_destroy').present?
  end
end
