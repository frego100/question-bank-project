# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  def index?
    # user.role.permissions.pluck(:name).include?('user_index')
    UserPermission.where(user_id: user.id, permission_name: 'user_index').present?
  end

  def show?
    # user.role.permissions.pluck(:name).include?('user_show')
    UserPermission.where(user_id: user.id, permission_name: 'user_show').present?
  end

  def new?
    # user.role.permissions.pluck(:name).include?('user_new')
    UserPermission.where(user_id: user.id, permission_name: 'user_new').present?
  end

  def edit?
    # user.role.permissions.pluck(:name).include?('user_edit')
    UserPermission.where(user_id: user.id, permission_name: 'user_edit').present?
  end

  def create?
    # user.role.permissions.pluck(:name).include?('user_create')
    UserPermission.where(user_id: user.id, permission_name: 'user_create').present?
  end

  def update?
    # user.role.permissions.pluck(:name).include?('user_update')
    UserPermission.where(user_id: user.id, permission_name: 'user_update').present?
  end

  def password_edit?
    # user.role.permissions.pluck(:name).include?('user_password_edit')
    UserPermission.where(user_id: user.id, permission_name: 'user_password_edit').present?
  end

  def destroy?
    # user.role.permissions.pluck(:name).include?('user_destroy')
    UserPermission.where(user_id: user.id, permission_name: 'user_destroy').present?
  end

  def pertinency_reviewers?
    # user.role.permissions.pluck(:name).include?('user_pertinency_reviewers')
    UserPermission.where(user_id: user.id, permission_name: 'user_pertinency_reviewers').present?
  end

  def format_reviewers?
    # user.role.permissions.pluck(:name).include?('user_format_reviewers')
    UserPermission.where(user_id: user.id, permission_name: 'user_format_reviewers').present?
  end

  ## OTROS METODOS OSCUROS
  def is_formulator?
    # record.role.permissions.pluck(:name).include?('complex_formulator_permission')
    UserPermission.where(user_id: user.id, permission_name: 'complex_formulator_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present?
  end

  def is_pertinency_reviewer?
    # record.role.permissions.pluck(:name).include?('complex_pertinency_reviewer_permission')
    UserPermission.where(user_id: user.id, permission_name: 'complex_pertinency_reviewer_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present?
  end

  def is_format_reviewer?
    # record.role.permissions.pluck(:name).include?('complex_format_reviewer_permission')
    UserPermission.where(user_id: user.id, permission_name: 'complex_format_reviewer_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present?
  end

  def is_operator?
    # record.role.permissions.pluck(:name).include?('complex_operator_permission')
    UserPermission.where(user_id: user.id, permission_name: 'complex_operator_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present?
  end
end
