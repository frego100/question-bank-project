# frozen_string_literal: true

class EntranceExamPolicy < ApplicationPolicy
  def index?
    # user.role.permissions.pluck(:name).include?('call_index')
    # UserPermission.where(user_id: user.id, permission_name: 'call_index').present?
    UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_operator_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'entrance_exam_index').present?
  end

  def show?
    # user.role.permissions.pluck(:name).include?('call_show')
    # UserPermission.where(user_id: user.id, permission_name: 'call_show').present?
    UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_operator_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'entrance_exam_show').present?
  end

  def new?
    # user.role.permissions.pluck(:name).include?('call_new')
    # UserPermission.where(user_id: user.id, permission_name: 'call_new').present?
    UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_operator_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'entrance_exam_new').present?
  end

  def edit?
    # user.role.permissions.pluck(:name).include?('call_edit')
    # UserPermission.where(user_id: user.id, permission_name: 'call_edit').present?
    UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_operator_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'entrance_exam_edit').present?
  end

  def create?
    # user.role.permissions.pluck(:name).include?('call_create')
    # UserPermission.where(user_id: user.id, permission_name: 'call_create').present?
    UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_operator_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'entrance_exam_create').present?
  end

  def update?
    # user.role.permissions.pluck(:name).include?('call_update')
    # UserPermission.where(user_id: user.id, permission_name: 'call_update').present?
    UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_operator_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'entrance_exam_update').present?
  end

  def destroy?
    # user.role.permissions.pluck(:name).include?('call_destroy')
    # UserPermission.where(user_id: user.id, permission_name: 'call_destroy').present?
    UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_operator_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'entrance_exam_destroy').present?
  end

  def add_question?
    # user.role.permissions.pluck(:name).include?('call_destroy')
    # UserPermission.where(user_id: user.id, permission_name: 'call_add_formulator').present?
    UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_operator_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'entrance_exam_add_question').present?
  end

  def remove_question?
    # user.role.permissions.pluck(:name).include?('call_destroy')
    # UserPermission.where(user_id: user.id, permission_name: 'call_add_formulator').present?
    UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_operator_permission').present? ||
      UserPermission.where(user_id: user.id, permission_name: 'entrance_exam_remove_question').present?
  end
end
