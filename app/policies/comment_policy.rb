# frozen_string_literal: true

class CommentPolicy < ApplicationPolicy
  def index?
    (UserPermission.where(user_id: user.id, permission_name: 'complex_pertinency_reviewer_permission') && record.pertinency_reviewer_id == user.id) ||
      (UserPermission.where(user_id: user.id, permission_name: 'complex_formulator_permission') && record.formulator_id == user.id) ||
      (UserPermission.where(user_id: user.id, permission_name: 'comment_index') && (record.pertinency_reviewer_id == user.id || record.formulator_id == user.id)) ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission')
  end

  def show?
    (UserPermission.where(user_id: user.id, permission_name: 'complex_pertinency_reviewer_permission') && record.pertinency_reviewer_id == user.id) ||
      (UserPermission.where(user_id: user.id, permission_name: 'comment_show') && (record.pertinency_reviewer_id == user.id || record.formulator_id == user.id)) ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission')
  end

  def create?
    (UserPermission.where(user_id: user.id, permission_name: 'complex_pertinency_reviewer_permission') && record.pertinency_reviewer_id == user.id) ||
      (UserPermission.where(user_id: user.id, permission_name: 'comment_create') && (record.pertinency_reviewer_id == user.id || record.formulator_id == user.id)) ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission')
  end

  def update?
    (UserPermission.where(user_id: user.id, permission_name: 'complex_pertinency_reviewer_permission') && record.pertinency_reviewer_id == user.id) ||
      (UserPermission.where(user_id: user.id, permission_name: 'comment_update') && (record.pertinency_reviewer_id == user.id || record.formulator_id == user.id)) ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission')
  end

  def destroy?
    (UserPermission.where(user_id: user.id, permission_name: 'complex_pertinency_reviewer_permission') && record.pertinency_reviewer_id == user.id) ||
      (UserPermission.where(user_id: user.id, permission_name: 'comment_destroy') && (record.pertinency_reviewer_id == user.id || record.formulator_id == user.id)) ||
      UserPermission.where(user_id: user.id, permission_name: 'complex_super_admin_permission')
  end
end
