# frozen_string_literal: true

class CallPolicy < ApplicationPolicy
  def index?
    # user.role.permissions.pluck(:name).include?('call_index')
    UserPermission.where(user_id: user.id, permission_name: 'call_index').present?
  end

  def show?
    # user.role.permissions.pluck(:name).include?('call_show')
    UserPermission.where(user_id: user.id, permission_name: 'call_show').present?
  end

  def new?
    # user.role.permissions.pluck(:name).include?('call_new')
    UserPermission.where(user_id: user.id, permission_name: 'call_new').present?
  end

  def edit?
    # user.role.permissions.pluck(:name).include?('call_edit')
    UserPermission.where(user_id: user.id, permission_name: 'call_edit').present?
  end

  def create?
    # user.role.permissions.pluck(:name).include?('call_create')
    UserPermission.where(user_id: user.id, permission_name: 'call_create').present?
  end

  def update?
    # user.role.permissions.pluck(:name).include?('call_update')
    UserPermission.where(user_id: user.id, permission_name: 'call_update').present?
  end

  def destroy?
    # user.role.permissions.pluck(:name).include?('call_destroy')
    UserPermission.where(user_id: user.id, permission_name: 'call_destroy').present?
  end

  def add_formulator?
    # user.role.permissions.pluck(:name).include?('call_destroy')
    UserPermission.where(user_id: user.id, permission_name: 'call_add_formulator').present?
  end

  def add_pertinency_reviewer?
    # user.role.permissions.pluck(:name).include?('call_destroy')
    UserPermission.where(user_id: user.id, permission_name: 'call_add_pertinency_reviewer').present?
  end

  def add_format_reviewer?
    # user.role.permissions.pluck(:name).include?('call_destroy')
    UserPermission.where(user_id: user.id, permission_name: 'call_add_format_reviewer').present?
  end

  def switch_status?
    UserPermission.where(user_id: user.id, permission_name: 'call_switch_status').present?
  end
end
