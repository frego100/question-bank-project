# frozen_string_literal: true

class SpecialityPolicy < ApplicationPolicy
  def index?
    # user.present?
    true
  end

  def create?
    # user.present?
    true
  end

  def update?
    # user.present?
    true
  end

  def destroy?
    # user.present?
    true
  end
end
