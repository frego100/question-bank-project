# frozen_string_literal: true

class QuestionPolicy < ApplicationPolicy
  def index?
    permissions = %w[complex_operator_permission question_index
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present?
  end

  def index_operator?
    permissions = %w[complex_operator_permission question_index_operator
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present?
  end

  def index_formulator?
    permissions = %w[complex_formulator_permission question_index_formulator
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present?
  end

  def index_reviewer?
    permissions = %w[complex_format_reviewer_permission
                     complex_pertinency_reviewer_permission question_index_reviewer
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present?
  end

  def show?
    permissions = %w[complex_pertinency_reviewer_permission question_show
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present?
  end

  def new?
    permissions = %w[complex_formulator_permission question_new
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present?
  end

  def edit?
    permissions = %w[complex_formulator_permission complex_format_reviewer_permission
                     question_edit complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present? &&
      (
        ([INCOMPLETE, OBSERVED].include?(record.status.to_sym) && record.formulator_id == user.id) ||
        (record.status.to_sym == ON_REVIEW && record.format_reviewer_id == user.id)
      )
  end

  def create?
    permissions = %w[complex_formulator_permission question_create
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present?
  end

  def update?
    permissions = %w[complex_formulator_permission complex_format_reviewer_permission question_update
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present? &&
      (
        ([INCOMPLETE, OBSERVED].include?(record.status.to_sym) && record.formulator_id == user.id) ||
        (record.status.to_sym == ON_REVIEW && record.format_reviewer_id == user.id)
      )
  end

  def destroy?
    permissions = %w[complex_formulator_permission question_destroy
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present?
  end

  def assign_question?
    permissions = %w[complex_operator_permission question_assign_question
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present?
  end

  def approve_question?
    permissions = %w[complex_format_reviewer_permission complex_operator_permission
                     complex_pertinency_reviewer_permission question_approve_question
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present? &&
      (record.status.to_sym == ON_REVIEW &&
        (record.pertinency_reviewer_id == user.id || record.format_reviewer_id == user.id ||
         UserPermission.where(user_id: user.id, permission_name: 'complex_operator_permission').present?))
  end

  def reject_question?
    permissions = %w[complex_operator_permission question_reject_question
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present? &&
      (record.status.to_sym == ON_REVIEW)
  end

  def send_question_to_review?
    permissions = %w[complex_formulator_permission question_send_question_to_review
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present? &&
      ([INCOMPLETE, OBSERVED].include?(record.status.to_sym) && record.formulator_id == user.id)
  end

  def observe_question?
    permissions = %w[question_observe_question complex_pertinency_reviewer_permission
                     complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).present? &&
      (record.status.to_sym == ON_REVIEW && record.pertinency_reviewer_id == user.id)
  end

  def formulator_status?
    permissions = %w[complex_formulator_permission question_formulator_status
                     complex_super_admin_permission]
    UserPermission.where(user_id: user.id, permission_name: permissions).exists?
  end

  def reviewer_status?
    permissions = %w[complex_pertinency_reviewer_permission complex_format_reviewer_permission
                     question_reviewer_status complex_super_admin_permission]

    UserPermission.where(user_id: user.id, permission_name: permissions).exists?
  end
end
