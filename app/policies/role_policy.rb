# frozen_string_literal: true

class RolePolicy < ApplicationPolicy
  def index?
    # user.role.permissions.pluck(:name).include?('role_index')
    UserPermission.where(user_id: user.id, permission_name: 'role_index').present?
  end

  def show?
    # user.role.permissions.pluck(:name).include?('role_show')
    UserPermission.where(user_id: user.id, permission_name: 'role_show').present?
  end

  def new?
    # user.role.permissions.pluck(:name).include?('role_new')
    UserPermission.where(user_id: user.id, permission_name: 'role_new').present?
  end

  def edit?
    # user.role.permissions.pluck(:name).include?('role_edit')
    UserPermission.where(user_id: user.id, permission_name: 'role_edit').present?
  end

  def create?
    # user.role.permissions.pluck(:name).include?('role_create')
    UserPermission.where(user_id: user.id, permission_name: 'role_create').present?
  end

  def update?
    # user.role.permissions.pluck(:name).include?('role_update')
    UserPermission.where(user_id: user.id, permission_name: 'role_update').present?
  end

  def destroy?
    # user.role.permissions.pluck(:name).include?('role_destroy')
    UserPermission.where(user_id: user.id, permission_name: 'role_destroy').present?
  end
end
