# frozen_string_literal: true

class FirstMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.first_mailer.first_mail.subject
  #
  def first_mail
    @greeting = 'Hi'

    mail to: 'to@example.org'
  end
end
