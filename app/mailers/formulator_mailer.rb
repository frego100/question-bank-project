# frozen_string_literal: true

class FormulatorMailer < ApplicationMailer
  default from: 'notifications@unsa.edu.pe'
  def summon_formulator_email
    @formulator = User.find(params[:formulator_id])
    @call = Call.find(params[:call_id])
    @user_call = UserCall.find(params[:user_call_id])
    @categories = @user_call.categories
    mail(to: @formulator.email, subject: 'Invitación para proceso de formulación')
  end

  def change_categories_email
    @formulator = User.find(params[:formulator_id])
    @call = Call.find(params[:call_id])
    @user_call = UserCall.find(params[:user_call_id])
    @categories = @user_call.categories
    mail(to: @formulator.email, subject: 'Notificación de cambio de categorías')
  end

  def observed_question_email
    @question = Question.find(params[:question_id])
    @formulator = @question.formulator
    @comments = @question.comments
    mail(to: @formulator.email, subject: 'Pregunta observada')
  end
end
