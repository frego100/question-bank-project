# README

National University of San Agustin - Universidad Nacional de San Agustín(UNSA)
Questions Bank that functions as a repository to store exams questions.

![](https://pandao.github.io/editor.md/images/logos/editormd-logo-180x180.png)

![](https://img.shields.io/github/stars/pandao/editor.md.svg) ![](https://img.shields.io/github/forks/pandao/editor.md.svg) ![](https://img.shields.io/github/tag/pandao/editor.md.svg) ![](https://img.shields.io/github/release/pandao/editor.md.svg) ![](https://img.shields.io/github/issues/pandao/editor.md.svg) ![](https://img.shields.io/bower/v/editor.md.svg)


**Table of Contents**

[TOCM]

[TOC]



## Project Description

This project is built using Ruby on Rails.
We will be using the following tools to software to set the project up in your local:

- Git
- Rbenv
- Postgresql
- Visual Studio Code
- Redis
- Mailcatcher

### Git

We will use git to control the project versions and to collaborate efficiently.

To clone the project run: ``` git clone <url> ```

### Rbenv

To manage Ruby versions we will use Rbenv in Linux/Mac

[Install Rbenv in Linux Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-18-04 "Install Rbenv in Linux Ubuntu 18.04")

To update rbenv list of Ruby versions use this: [Update Rbenv List](https://gist.github.com/popac/b7897486f16417afb8de9664cae2bc1b "Update Rbenv List")

#### Ruby and Ruby on Rails

Use this documentation for learning to install ruby versions: [Rbenv Docs](https://github.com/rbenv/rbenv "Rbenv Docs")

We will use Ruby 2.6.5

Install Ruby on Rails using: ``` gem install rails -v 6.3.0```

#### Rubocop

We will use rubocop to format our code, you can do this by running: ```gem install rubocop```

#### Mailcatcher

We will use mailcatcher as a local email server, this will be useful in the development process, just run: ```gem install mailcatcher```

### Postgresql

Use this tutorial to install Postgresql in your local: [Install Postgresql Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04 "Install Postgresql Ubuntu 18.04")

### Visual Studio Code

We will use VSCode as our code Editor, you will need to set your plugins right to start writing code of quality.

#### VSCode

Download it from: [VSCode Page](https://code.visualstudio.com/ "VSCode Page")

You will need to install some plugins for helping you to create code of quality:

- ESLint
- Prettier - Code Formatter
- Ruby
- Rails
- ruby-rubocop
- Babel Javascript
- Babel ES6/ES7

And also you will need to configure VSCode to format the code automatically after saving.

Set the settings.json like this:
```json
{
    "workbench.iconTheme": "vscode-icons",
    "editor.formatOnSave": false,
    "editor.tabSize": 2,
    "editor.fontSize": 14,
    "[javascript]": {
        "editor.formatOnSave": true,
    },
    "[javascriptreact]": {
        "editor.formatOnSave": true,
    },
    "ruby.rubocop.executePath": "",
    "ruby.rubocop.configFilePath": "./.rubocop.yml",
    "ruby.rubocop.onSave": true,
    "ruby.format": "rubocop",
    "[ruby]": {
        "editor.formatOnSave": true
    },
    "git.autofetch": true
}
```

After this you will be able to format your code automatically just by saving.

To save without formatting a file you will need to use ```Ctrl + K + S``` instead of ```Ctrl + S```. You will use this specially in config files.

### Redis

You can install Redis running: ```sudo apt-get install redis-server```

Run redis from your terminal: ```redis-server```

### Mailcatcher

To run mailcatcher use: ```mailcatcher``` in a terminal.

Here is the documentation: [Mailcatcher](https://mailcatcher.me/ "Mailcatcher")

### Set the email server locally

You need to install mailcatcher with: ```gem install mailcatcher```

Set up your rails app, I recommend adding this to your ```environments/development.rb:```

```config.action_mailer.delivery_method = :smtp```

```config.action_mailer.smtp_settings = { :address => "localhost", :port => 1025 }```